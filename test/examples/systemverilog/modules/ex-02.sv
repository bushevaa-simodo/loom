﻿module ex(
  input logic in_value,  
  output logic out_value
);
  always_ff @ (posedge in_value)
  begin
    out_value <= 'b1;
  end  
endmodule
