﻿// Write your modules here! 
module fsm(
  input logic clk,  
  input logic rst,
  input logic [5:0] c,
  output logic [2:0] st,
  output logic [7:0] num
);
  logic [2:0] State;  
  always_comb
    st = State;
  always_ff @ (posedge clk, negedge rst)
  begin
    if(!rst) begin      
      num <= 0;
      State <= 1;
    end
    else if(State == 1) begin      
      if(c[4] == 1 && c[5] == 1) begin
        num <= 'b00000101;        
        State <= 2;
      end      
      else if(c[2] == 1 && c[3] == 0) begin  
        num <= 'b00011000;        
        State <= 4;
      end      
      else if(c[0] == 1 || c[2] == 0) begin
        num <= 'b01100000;        
        State <= 6;
      end      
      else if(c[3] == 1)
        State <= 5;      
      else begin
        num <= 'b00001000;        
        State <= 1;
      end    
    end
    else if(State == 2) begin      
      if(c[0] == 1 && c[1] == 1 && c[2] == 1)
        State <= 2;      
      else
        State <= 3;    
    end
    else if (State == 3) begin      
      if(c[0] == 1 && c[1] == 0) begin
        num <= 'b10010000;        
        State <= 1;
      end      
      else begin
       num <= 'b00000001;        
       State <= 3;
      end   
    end
    else if(State == 4) begin      
      if(c[0] == 1 && c[5] == 0)
        State <= 4;      
      else begin
        num <= 'b10001000;        
        State <= 3;
      end    
    end
    else if(State == 5) begin      
      if (c[0] == 1 && c[1] == 1 && c[2] == 1) begin
       num <= 'b00000001;        
        State <= 1;
      end      
      else
        State <= 5;    
    end
    else if(State == 6) begin      
      if (c[4] == 1 || c[5] == 1) begin
        num <= 'b00000001;        
        State <= 5;
      end      
      else
        State <= 6;    
    end
  end  
endmodule

