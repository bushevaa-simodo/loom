#include "GlobalMessageView.h"

#include <QtGui>

void GlobalMessageView::wheelEvent(QWheelEvent *event)
{
    if (event->modifiers() != Qt::ControlModifier) {
        QListView::wheelEvent(event);
        return;
    }

    QFont f = font();

    int point_size_delta = event->angleDelta().y() > 0 ? 1 : -1;
    int point_size       = f.pointSize() + point_size_delta;

    if (point_size > 0 && point_size < 30) {
        f.setPointSize(point_size);
        setFont(f);
    }
}
