#ifndef GlobalMessages_H
#define GlobalMessages_H

#include "GlobalMessageModel.h"
#include "GlobalMessageView.h"

#include "simodo/shell/access/Access_interface.h"

#include <QWidget>
#include <QList>

#include <map>
#include <vector>
#include <list>
#include <functional>

QT_BEGIN_NAMESPACE
class QVBoxLayout;
class QListView;
class QPalette;
class QCheckBox;
class QDockWidget;
QT_END_NAMESPACE

typedef std::function<bool(size_t button_number)> onClickFunction;

struct AskButton
{
    QString                title;
    QString                description;
    onClickFunction        on_click_function = nullptr;
};

struct GlobalAsk
{
    shell::MessageSeverity severity;
    QString                title;
    QString                description;
    std::vector<AskButton> buttons;
    QString                group_name;
};

class GlobalMessages : public QWidget
{
    Q_OBJECT

    /// @attention Убедитесь в кросскомпилируемости при использовании циклических ссылок на MainWindow
    QDockWidget &        _dock_widget;

    GlobalMessageModel  _model;

    QVBoxLayout *       _layout;
    QListView *         _list;
    QWidget *           _ask_widget = nullptr;
    QCheckBox *         _auto_repeat;

    QIcon               _warning_icon;

    QList<GlobalMessageModel::Item> _message_buffer;
    int                 _delay_timer = -1;

    std::list<GlobalAsk> _ask_queue;
    std::map<QString,size_t> _group_action;

public:
    GlobalMessages(QDockWidget & dock_widget);

    void sendGlobal(const QString & text, shell::MessageSeverity severity=shell::MessageSeverity::Info);
    void askGlobal(shell::MessageSeverity severity, const QString & title, const QString & description, 
                   std::vector<AskButton> buttons, const QString & group_name = "");

protected:
    virtual void timerEvent(QTimerEvent *event) override;
    virtual bool event(QEvent * event) override;

private:
    QString setupItemAndGetPrefix(QStandardItem *item, shell::MessageSeverity severity);
    void setPaletteColors(const QPalette & palette);
    void flushBuffer();
    void addItemToModel(const QString & text, shell::MessageSeverity severity);
    void resetTimer();
    void showAsk();
    void closeAsk();
};

#endif // GlobalMessages_H