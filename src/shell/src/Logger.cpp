#include "Logger.h"
#include "MainWindow.h"

void Logger::output(shell::MessageSeverity level, const std::string & message, const std::string & context)
{
    if (level >= _min_level) {
        /// @todo Нужно перенести контроль критической секции в _main_window->sendGlobal. К тому же
        /// можно её реализовать через сигналы-слоты
        static std::mutex logger_output_mutex;
        std::lock_guard   locker(logger_output_mutex);
        _main_window->sendGlobal(QString::fromStdString(_logger_name + ": " + message), 
                                 level);
        if (!context.empty()) {
            if (context.length() > 1000) {
                std::string cropped = context.substr(0,1000) + "...";
                _main_window->sendGlobal(QString::fromStdString(_logger_name + ": " + cropped), 
                                        shell::MessageSeverity::Info);
            }
            else
                _main_window->sendGlobal(QString::fromStdString(_logger_name + ": " + context), 
                                        shell::MessageSeverity::Info);
        }
    }
}

