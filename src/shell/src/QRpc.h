#ifndef QRpc_H
#define QRpc_H

#include <QJsonDocument>

class QJsonRpc
{
    QJsonDocument   _doc;
    bool            _is_valid  = false;
    QString         _jsonrpc_version;
    QString         _method;
    int             _id = -1;

    QJsonValue          _fail;
    const QJsonValue *  _params = nullptr;
    const QJsonValue *  _result = nullptr;
    const QJsonValue *  _error = nullptr;

public:
    QJsonRpc() = default;
    explicit QJsonRpc(const std::string & json);
    explicit QJsonRpc(const QString & json);
    explicit QJsonRpc(QJsonDocument doc);
    explicit QJsonRpc(QString method, QJsonValue params, int id);
    explicit QJsonRpc(QString method, int id);
    explicit QJsonRpc(QString method, QJsonValue params);
    // Rpc(QString method); // Конфликтует с первым конструктором
    explicit QJsonRpc(QJsonValue result, int id);
    explicit QJsonRpc(int code, QString message, QJsonValue data, int id);
    explicit QJsonRpc(int code, QString message, int id);

    const QJsonDocument&doc()       const { return _doc; }
    bool                is_valid()  const { return _is_valid; }
    const QString &     jsonrpc_version()   const { return _jsonrpc_version; }
    const QString &     method()    const { return _method; }
    const QJsonValue &  params()    const;
    const QJsonValue &  result()    const;
    const QJsonValue &  error()     const;
    int                 id()        const { return _id; }

private:
    void setupMembers();
};

#endif