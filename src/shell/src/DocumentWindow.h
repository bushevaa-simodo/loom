#ifndef DocumentWindow_H
#define DocumentWindow_H

#include "simodo/shell/document/ViewAdaptor_interface.h"
#include "MainWindow.h"

#include <QWidget>

class QVBoxLayout;
class QListWidget;
class QSplitter;

struct TabParameters
{
    shell::ViewAdaptor_interface *  view;
    QWidget *                       widget = nullptr;
    unsigned                        orientation = 0;
};

/**
 * @brief Виджет окна документа
 *
 */
class DocumentWindow : public QWidget
{
    Q_OBJECT

    int                     _current_tab = 0;
    MainWindow *            _main_window;
    QVector<TabParameters>  _tab_params;
    QVBoxLayout *           _layout      = nullptr;
    QListWidget *           _diagnostics;
    QSplitter *             _main_splitter;
    bool                    _diagnostics_size_trigger = false;
    QString                 _current_file;
    bool                    _is_untitled = true;

public:
    DocumentWindow(MainWindow * main_window, QVector<shell::ViewAdaptor_interface *> views);
    ~DocumentWindow();

    const QVector<TabParameters> & tab_parameters() const { return _tab_params; }

    unsigned orientation() const { return _tab_params[_current_tab].orientation; }

    bool save();
    bool saveAs();
    QString userFriendlyCurrentFile();
    QString currentFile() const { return _current_file; }
    // void newFile(const QString & path);
    void setCurrentFile(const QString &fileName);

    shell::ViewAdaptor_interface * view_adaptor() const { return _tab_params[_current_tab].view; }
    std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor() const { return _tab_params[_current_tab].view->document_adaptor(); }

    QWidget * document_widget() { return _tab_params[_current_tab].view->document_widget(); }

    bool loadFile(const QString &fileName);
    bool saveFile(const QString &fileName);

    // bool openView(const std::vector<simodo::lsp::SimodoCommandReport> & reports);

    void print(QPrinter * printer) { _tab_params[_current_tab].view->print(printer); }

    void cutFromDocument() { _tab_params[_current_tab].view->cutFromDocument(); }
    void copyFromDocument() { _tab_params[_current_tab].view->copyFromDocument(); }
    void pasteToDocument() {_tab_params[_current_tab].view->pasteToDocument(); }
    bool hasSelection() { return _tab_params[_current_tab].view->hasSelection(); }
    bool hasUndo() { return _tab_params[_current_tab].view->hasUndo(); }
    bool hasRedo() { return _tab_params[_current_tab].view->hasRedo(); }

    void setLineCol(QPair<int,int> line_col) { _tab_params[_current_tab].view->setLineCol(line_col); }

    void zoomIn() { _tab_params[_current_tab].view->zoomIn(); }
    void zoomOut() { _tab_params[_current_tab].view->zoomOut(); }
    void zoomZero() { _tab_params[_current_tab].view->zoomZero(); }

    shell::service_t supports() const { return _tab_params[_current_tab].view->plugin()->supports(); }

    void publishDiagnostics(int version, QVector<shell::Diagnostic> &diagnostics, int checksum);

public slots:
    void regroupViews(unsigned orientation=0);

protected:
    virtual void closeEvent(QCloseEvent *event) override;
    virtual void wheelEvent(QWheelEvent *e) override;

private:
    bool maybeSave();
    QString strippedName(const QString &fullFileName);

private slots:
    void onTabChanged(int tab_no);

    friend class ShellAccess;
};

#endif // DocumentWindow_H
