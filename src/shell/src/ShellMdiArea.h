#ifndef ShellMdiArea_H
#define ShellMdiArea_H

#include <QMdiArea>

#include <vector>

QT_BEGIN_NAMESPACE
class QListWidget;
class QFont;
QT_END_NAMESPACE

class MainWindow;
class DocumentWindow;
class DocumentList;

class ShellMdiArea : public QMdiArea
{
    MainWindow &            _main;
    DocumentList *          _document_list;

    std::vector<QString>    _history;
    QFont                   _last_tooltip_font;

public:
    ShellMdiArea(MainWindow * main);

    DocumentWindow * findDocumentWindow(const QString &file_name) const;
    const std::vector<QString> & history() const { return _history; }

protected:
    virtual void keyReleaseEvent(QKeyEvent *event) override;
    virtual bool eventFilter(QObject *object, QEvent *event) override;

private slots:
    void subWindowActivated_slot(QMdiSubWindow *window);
    void activated_slot(const QModelIndex &index);
};

#endif // ShellMdiArea_H
