#ifndef Logger_H
#define Logger_H

#include "simodo/shell/access/Access_interface.h"
#include "simodo/inout/log/Logger_interface.h"

#include <iostream>

namespace shell = simodo::shell;

class MainWindow;

class Logger: public simodo::inout::Logger_interface
{
    MainWindow *            _main_window;
    std::string             _logger_name;
    shell::MessageSeverity  _min_level;

    void output(shell::MessageSeverity level, const std::string & message, const std::string & context);

public:
    Logger() = delete;
    Logger(MainWindow * main_window, std::string name, shell::MessageSeverity min_level=shell::MessageSeverity::Info) 
        : _main_window(main_window)
        , _logger_name(name) 
        , _min_level(min_level)
    {}

    virtual const std::string & name() const override 
    { return _logger_name; }
    virtual void debug(const std::string & message, const std::string & context="") override 
    { output(shell::MessageSeverity::Debug, message, context); }
    virtual void info(const std::string & message, const std::string & context="") override
    { output(shell::MessageSeverity::Info, message, context); }
    virtual void warning(const std::string & message, const std::string & context="") override
    { output(shell::MessageSeverity::Warning, message, context); }
    virtual void error(const std::string & message, const std::string & context="") override
    { output(shell::MessageSeverity::Error, message, context); }
    virtual void critical(const std::string & message, const std::string & context="") override
    { output(shell::MessageSeverity::Error, message, context); }
};


#endif
