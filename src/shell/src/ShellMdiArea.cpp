#include "ShellMdiArea.h"
#include "MainWindow.h"
#include "DocumentWindow.h"

#include <QtWidgets>

#include <algorithm>

class DocumentList: public QListWidget
{
public: 
    DocumentList(QWidget * parent) : QListWidget(parent) {}

    /// \todo Иногда при переходе фокуса на другой объект не закрывается окно при отпускании Ctrl.
    /// Попытка управлять через метод focusOutEvent не помогло. Желательно разобраться в проблеме
    // virtual void focusOutEvent(QFocusEvent * event) override
    // {
    //     QListWidget::focusOutEvent(event);

    //     if (event->lostFocus() && !isHidden())
    //         hide();
    // }
};


ShellMdiArea::ShellMdiArea(MainWindow * main)
    : QMdiArea(main)
    , _main(*main)
{
    Q_ASSERT(main);

    setViewMode(QMdiArea::TabbedView);
    setTabsClosable(true);
    setDocumentMode(true);
    setTabPosition(QTabWidget::West);
    setActivationOrder(QMdiArea::ActivationHistoryOrder);
    // setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);

    _document_list = new DocumentList(this);
    _document_list->setWindowFlags(Qt::Tool | Qt::FramelessWindowHint);
    _document_list->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _document_list->hide();

    connect(this, &QMdiArea::subWindowActivated, this, &ShellMdiArea::subWindowActivated_slot);
    connect(_document_list, &QAbstractItemView::activated, this, &ShellMdiArea::activated_slot);
    // connect(_document_list, &QWidget::foc, this, &ShellMdiArea::activated_slot);

    installEventFilter(main);
}

DocumentWindow* ShellMdiArea::findDocumentWindow(const QString& file_name) const
{
    QString canonicalFilePath   = (file_name == "Setup")
                                ? file_name
                                : QFileInfo(file_name).canonicalFilePath();

    const QList<QMdiSubWindow *> subWindows = subWindowList();
    for (QMdiSubWindow *subwin : subWindows) {
        DocumentWindow *document_window = qobject_cast<DocumentWindow *>(subwin->widget());
        if (document_window && document_window->currentFile() == canonicalFilePath)
            return document_window;
    }
    return nullptr;
}

void ShellMdiArea::keyReleaseEvent(QKeyEvent* event)
{
    if(event->key() == Qt::Key_Control && !_document_list->isHidden()) {
        activated_slot(_document_list->currentIndex());
    }

    QMdiArea::keyReleaseEvent(event);
}

bool ShellMdiArea::eventFilter(QObject* object, QEvent* event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent * key_event = static_cast<QKeyEvent *>(event);
        
        if (key_event->key() == Qt::Key_Escape) {
            if (!_document_list->isHidden()) {
                _document_list->hide();
                return true;
            }
        }
        else if ((key_event->key() == Qt::Key_Tab || key_event->key() == Qt::Key_Backtab)
         && key_event->modifiers().testFlag(Qt::ControlModifier)) {
            if (_document_list->isHidden()) {
                _document_list->clear();

                for(const QString & file_path : _history) {
                    DocumentWindow * document_window = findDocumentWindow(file_path);
                    if (document_window) {
                        QIcon icon = _main.filesystem_model().fileIcon(
                                        _main.filesystem_model().index(document_window->currentFile()));

                        if (icon.isNull())
                            icon = document_window->view_adaptor()->plugin()->icon();

                        if (icon.isNull())
                            icon = windowIcon();

                        QListWidgetItem * item = new QListWidgetItem(
                                                        icon, 
                                                        document_window->userFriendlyCurrentFile());
                        item->setToolTip(document_window->currentFile());
                        _document_list->insertItem(0,item);
                    }
                }

                if (_document_list->count() > 0) {
                    _document_list->setCurrentRow(0);

                    const int max_visible_rows = 12;
                    int visible_rows = qMin(_document_list->count(), max_visible_rows);
                    int w = _document_list->sizeHintForColumn(0) 
                                + (_document_list->count() > max_visible_rows ? _document_list->verticalScrollBar()->sizeHint().width() : 0)
                                + 2 * _document_list->frameWidth();
                    int h = _document_list->sizeHintForRow(0) * visible_rows 
                                + 2 * _document_list->frameWidth();
                    _document_list->setFixedSize(w, h);

                    QRect mg = _main.geometry();
                    _document_list->move(mg.x() + mg.width()/2 - w/2, mg.y() + mg.height() / 2 - h/2);

                    _document_list->show();

                    _last_tooltip_font = QToolTip::font();
                    QToolTip::setFont(_document_list->font());
                }
            }

            if (_document_list->count() > 0) {
                if (key_event->key() == Qt::Key_Tab) {
                    int row = _document_list->currentRow() + 1;
                    if (row >= _document_list->count())
                        row = 0;
                    _document_list->setCurrentRow(row);                
                }
                else if (key_event->key() == Qt::Key_Backtab) {
                    int row = _document_list->currentRow() - 1;
                    if (row < 0)
                        row = _document_list->count() - 1;
                    _document_list->setCurrentRow(row);                
                }
            }

            return true;
        }
    }

    return QMdiArea::eventFilter(object, event);
}

void ShellMdiArea::subWindowActivated_slot(QMdiSubWindow *window)
{
    if (window) {
        DocumentWindow * document_window = qobject_cast<DocumentWindow *>(window->widget());
        if (document_window) {
            auto it = std::find(_history.begin(),_history.end(), document_window->currentFile());
            if (it != _history.end())
                _history.erase(it);
            _history.push_back(document_window->currentFile());
        }
    }
}

void ShellMdiArea::activated_slot(const QModelIndex &index)
{
    QListWidgetItem * item      = _document_list->item(index.row());
    QString           file_name = item->toolTip();

    _document_list->hide();
    QToolTip::setFont(_last_tooltip_font);

    if (file_name == "Setup") {
        const QList<QMdiSubWindow *> subWindows = subWindowList();
        for (QMdiSubWindow * subwin : subWindows) {
            DocumentWindow * document_window = qobject_cast<DocumentWindow *>(subwin->widget());
            if (document_window && document_window->currentFile() == "Setup") {
                setActiveSubWindow(subwin);
                break;
            }
        }
    }
    else {
        _main.openFile(file_name);
    }
}

