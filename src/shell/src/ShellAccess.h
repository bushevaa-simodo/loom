#ifndef ShellAccess_H
#define ShellAccess_H

#include "LspManagement.h"

#include "simodo/shell/access/Access_interface.h"
#include "simodo/shell/document/ViewAdaptor_interface.h"

#include "simodo/module/ModuleCollector.h"

class MainWindow;

namespace shell = simodo::shell;

class ShellAccess : public QObject, public shell::Access_interface
{
    Q_OBJECT

    MainWindow *                    _main_window;
    simodo::module::ModuleCollector _module_collector;

public:
    ShellAccess(MainWindow * main_window);

public:
    virtual QFileSystemModel & filesystem_model() override;

    virtual void sendGlobal(const QString & text, shell::MessageSeverity severity=shell::MessageSeverity::Info) override;
    virtual bool openFile(const QString & path) override;

    virtual const QAction * edit_undo_action() const override;
    virtual const QAction * edit_redo_action() const override;
#ifndef QT_NO_CLIPBOARD
    virtual const QAction * cut_action() const override;
    virtual const QAction * copy_action() const override;
#endif

    virtual void displayCursorPosition(const QString & info) override;
    virtual void displayStatistics(const QString & info) override;

    virtual const std::shared_ptr<shell::DocumentAdaptor_interface> getDocumentAdaptor(const QString & path, bool create=true) override;
    virtual std::optional<QString> getDocumentAdaptorPath(const std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor) override;

    virtual const shell::ViewAdaptor_interface * getCurrentViewAdaptor() override;
    virtual void setCurrentDocumentWindowModified(bool modified) override;

    virtual bool createPanel(const QString & panel_type, const QString & panel_name, const QString & title) override;
    virtual bool risePanel(const QString & panel_name, const QString &title) override;
    virtual bool setPanelTitle(const QString & panel_name, const QString & title) override;
    virtual bool sendDataToPanel(const QString & panel_name, const QString &title, const QJsonObject & value) override;
    virtual bool sendDataToCurrentDocument(const QJsonObject & value) override;
    virtual QString provideDataStorageFolder(const shell::Plugin_interface * plugin) override;
    virtual QString provideDataStorageFolder(const QString & feature_name) override;
    virtual QString performAllMacroSubstitutions(const QString & text) override;
    virtual simodo::module::ModuleCollector_interface * module_collector() override { return &_module_collector; }
    virtual shell::LspAccess_interface * getLspAccess(const QString & path_to_file) override;
    virtual shell::RunnerManagement_interface * getRunnerManagement() override;
    virtual void notifyContentChanged(const QString & path_to_file) override;
};

#endif // ShellAccess_H
