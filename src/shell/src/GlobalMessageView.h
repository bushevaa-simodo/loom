#ifndef GlobalMessageView_H
#define GlobalMessageView_H

#include <QtCore>
#include <QtWidgets>

class GlobalMessageView : public QListView
{
public:
    GlobalMessageView() = default;

protected:
    void wheelEvent(QWheelEvent *event) override;
};

#endif // GlobalMessageView_H