#ifndef DocumentCollector_H
#define DocumentCollector_H

#include "simodo/shell/document/DocumentAdaptor_interface.h"

#include <QMap>
#include <QFileSystemWatcher>
#include <QAction>

#include <optional>

class MainWindow;

namespace shell  = simodo::shell;

class DocumentCollector : public QObject
{
    Q_OBJECT

    MainWindow *                _main_window;
    QMap<QString,std::shared_ptr<shell::DocumentAdaptor_interface>> _docs;
    QFileSystemWatcher          _watcher;

public:
    DocumentCollector(MainWindow * main_window);
    ~DocumentCollector();

    bool add(const QString & path, std::shared_ptr<shell::DocumentAdaptor_interface> doc);
    bool remove(const QString & path);

public:
    std::shared_ptr<shell::DocumentAdaptor_interface> find(const QString & path) const;
    std::optional<QString> adaptorPath(const std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor) const;

};

#endif // DocumentCollector_H