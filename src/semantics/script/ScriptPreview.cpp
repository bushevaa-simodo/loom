/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "ScriptPreview.h"

namespace simodo::interpret
{
    ScriptPreview::ScriptPreview(ModuleManagement_interface & module_management)
        : ScriptSemantics_abstract(module_management)
    {
    }

    bool ScriptPreview::checkInterpretType(InterpretType interpret_type) const
    {
        return interpret_type == InterpretType::Preview;
    }

}