/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "ScriptOperationCode.h"

namespace simodo::interpret
{
    std::string getSblOperationCodeName(ScriptOperationCode op) noexcept
    {
        std::string str;

        switch(op)
        {
        case ScriptOperationCode::None:
            str = "None";
            break;
        case ScriptOperationCode::PushConstant:
            str = "PushConstant";
            break;
        case ScriptOperationCode::PushVariable:
            str = "PushVariable";
            break;
        case ScriptOperationCode::ObjectElement:
            str = "ObjectElement";
            break;
        case ScriptOperationCode::FunctionCall:
            str = "FunctionCall";
            break;
        case ScriptOperationCode::ProcedureCheck:
            str = "ProcedureCheck";
            break;
        case ScriptOperationCode::Print:
            str = "Print";
            break;
        case ScriptOperationCode::Block:
            str = "Block";
            break;
        case ScriptOperationCode::ArrayElement:
            str = "ArrayElement";
            break;
        case ScriptOperationCode::Contract:
            str = "ContractDefinition";
            break;
        case ScriptOperationCode::Type:
            str = "TypeDefinition";
            break;
        case ScriptOperationCode::Announcement:
            str = "Announcement";
            break;
        case ScriptOperationCode::Import:
            str = "Import";
            break;
        case ScriptOperationCode::Declaration:
            str = "Declaration";
            break;
        case ScriptOperationCode::ObjectStructure:
            str = "RecordStructure";
            break;
        case ScriptOperationCode::ArrayStructure:
            str = "ArrayStructure";
            break;
        case ScriptOperationCode::Initialize:
            str = "Initialize";
            break;
        case ScriptOperationCode::Assignment:
            str = "Assignment";
            break;
        case ScriptOperationCode::Plus:
            str = "Plus";
            break;
        case ScriptOperationCode::Minus:
            str = "Minus";
            break;
        case ScriptOperationCode::Or:
            str = "Or";
            break;
        case ScriptOperationCode::And:
            str = "And";
            break;
        case ScriptOperationCode::Equal:
            str = "Equal";
            break;
        case ScriptOperationCode::NotEqual:
            str = "NotEqual";
            break;
        case ScriptOperationCode::Less:
            str = "Less";
            break;
        case ScriptOperationCode::LessOrEqual:
            str = "LessOrEqual";
            break;
        case ScriptOperationCode::More:
            str = "More";
            break;
        case ScriptOperationCode::MoreOrEqual:
            str = "MoreOrEqual";
            break;
        case ScriptOperationCode::Addition:
            str = "Addition";
            break;
        case ScriptOperationCode::Subtraction:
            str = "Subtraction";
            break;
        case ScriptOperationCode::Multiplication:
            str = "Multiplication";
            break;
        case ScriptOperationCode::Division:
            str = "Division";
            break;
        case ScriptOperationCode::Modulo:
            str = "Modulo";
            break;
        case ScriptOperationCode::Power:
            str = "Power";
            break;
        case ScriptOperationCode::Ternary:
            str = "Ternary";
            break;
        case ScriptOperationCode::If:
            str = "If";
            break;
        case ScriptOperationCode::MeasureUnit:
            str = "MeasureUnit";
            break;
        case ScriptOperationCode::MeasureRatio:
            str = "MeasureRatio";
            break;
        case ScriptOperationCode::AssignmentAddition:
            str = "AssignmentAddition";
            break;
        case ScriptOperationCode::AssignmentSubtraction:
            str = "AssignmentSubtraction";
            break;
        case ScriptOperationCode::AssignmentMultiplication:
            str = "AssignmentMultiplication";
            break;
        case ScriptOperationCode::AssignmentDivision:
            str = "AssignmentDivision";
            break;
        case ScriptOperationCode::AssignmentModulo:
            str = "AssignmentModulo";
            break;
        case ScriptOperationCode::FunctionDefinition:
            str = "FunctionDefinition";
            break;
        case ScriptOperationCode::FunctionDefinitionEnd:
            str = "FunctionDefinitionEnd";
            break;
        case ScriptOperationCode::Break:
            str = "Break";
            break;
        case ScriptOperationCode::Continue:
            str = "Continue";
            break;
        case ScriptOperationCode::Return:
            str = "Return";
            break;
        case ScriptOperationCode::For:
            str = "For";
            break;
        case ScriptOperationCode::While:
            str = "While";
            break;
        case ScriptOperationCode::DoWhile:
            str = "DoWhile";
            break;
        case ScriptOperationCode::Apply:
            str = "Apply";
            break;
        case ScriptOperationCode::Using:
            str = "Using";
            break;
        default:
            str = "***";
            break;
        }

        return str;
    }
}