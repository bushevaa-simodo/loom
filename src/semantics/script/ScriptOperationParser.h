/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_sbl_OperationParser
#define simodo_sbl_OperationParser

/*! \file OperationParser.h
    \brief .
*/

#include "simodo/ast/Node.h"
#include "simodo/interpret/InterpretState.h"

#include <string>

namespace simodo::interpret
{
    class ScriptSemantics_abstract;

    typedef InterpretState (*OperationParser_t) (ScriptSemantics_abstract &, const ast::Node &);

    namespace OperationParser
    {
        InterpretState   parseNone               (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parsePushConstant       (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parsePushVariable       (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseObjectElement      (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseFunctionCall       (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseProcedureCheck     (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parsePrint              (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseBlock              (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parsePop                (ScriptSemantics_abstract &host, const ast::Node & op);
        
        InterpretState   parseReference          (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseArrayElement       (ScriptSemantics_abstract &host, const ast::Node & op);

        InterpretState   parseRecordStructure    (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseArrayStructure     (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseImport             (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseContractDefinition (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseAnnouncement       (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseDeclaration        (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseDeclarationCompletion(ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseAssignment         (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseGroupInitialize    (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parsePostAssignment     (ScriptSemantics_abstract &host, const ast::Node & op);

        InterpretState   parseUnary              (ScriptSemantics_abstract &host, const ast::Node & op);

        InterpretState   parseLogical            (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseCompare            (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseArithmetic         (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseConditional        (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseFunctionDefinition (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseFunctionDefinitionEnd(ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseFunctionTethered   (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseReturn             (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseFor                (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseWhile              (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseDoWhile            (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseBreak              (ScriptSemantics_abstract &host, const ast::Node & op);
        InterpretState   parseContinue           (ScriptSemantics_abstract &host, const ast::Node & op);

        InterpretState   parseApply              (ScriptSemantics_abstract &host, const ast::Node & op);
        // InterpretState   parseAutoDefine         (ScriptSemantics_abstract &host, const ast::Node & op);

        InterpretState   parseFiberOperations    (ScriptSemantics_abstract &host, const ast::Node & op);

        InterpretState   parseCheckState         (ScriptSemantics_abstract &host, const ast::Node & op);
    }

}

#endif // simodo_sbl_OperationParser
