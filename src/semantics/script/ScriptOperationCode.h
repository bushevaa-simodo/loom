/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_sbl_OperationCode
#define simodo_sbl_OperationCode

/*! \file ScriptOperationCode.h
    \brief Мнемокоды операций языка SBL
*/

#include <string>
#include <cstdint>

namespace simodo::interpret
{
    inline const std::u16string SCRIPT_HOST_NAME {};

    /*! \brief Мнемокоды операций языка SBL

        Начальные операции обеспечивают работу подмножества SBL, которое необходимо для раскрутки 
        (см. class FuzeSubsetSblRdp).

     */
    enum class ScriptOperationCode : uint16_t
    {
        #include "sbl"

        // Последняя операция (определяет кол-во операций)
        LastOperation,

        #include "sbl1"
    };

    /*!
     * \brief Преобразовывает мнемокод операции в строку
     * \param op Мнемокод операции
     * \return   Строковое представление мнемокода операции
     */
    std::string getSblOperationCodeName(ScriptOperationCode op) noexcept;

}

#endif // simodo_sbl_OperationCode
