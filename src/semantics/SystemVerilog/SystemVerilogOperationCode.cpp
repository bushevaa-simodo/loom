/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "SystemVerilogOperationCode.h"

namespace sv
{
    std::u16string getSblOperationCodeName(SystemVerilogOperationCode op) noexcept
    {
        std::u16string str;

        switch(op)
        {
        case SystemVerilogOperationCode::None:
            str = u"None";
            break;
        case SystemVerilogOperationCode::PushArrayConst:
            str = u"PushArrayConst";
            break;
        default:
            str = u"***";
            break;
        }

        return str;
    }
}