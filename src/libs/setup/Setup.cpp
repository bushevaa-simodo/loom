/**
 * @file Setup.cpp
 * @author Michael Fetisov (fetisov.michael@bmstu.ru)
 * @brief 
 * @version 0.1
 * @date 2022-08-26
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "simodo/setup/Setup.h"
#include "simodo/setup/SetupJsonBuilder.h"

#include <algorithm>

namespace simodo::setup
{
    Setup::Setup(std::string default_setup_file, std::string work_setup_file)
        : _default_setup_file(default_setup_file)
        , _work_setup_file(work_setup_file)
    {
        _builder = std::make_unique<SetupJsonBuilder>();
    }

    bool Setup::load()
    {
        if (!_builder)
            return false;

        _description = _builder->build(_default_setup_file);

        return _builder->errors().empty();
    }

    bool simodo::setup::Setup::loadDefault()
    {
        return false;
    }

    bool simodo::setup::Setup::save()
    {
        return false;
    }

    SetupStructure Setup::structure(const std::string &id) const
    {
        const auto it = std::find_if(_description.setup.begin(), _description.setup.end(), 
                                    [id](const SetupStructure & s){
                                        return s.id == id;
                                    });

        return it == _description.setup.end() ? SetupStructure {} : *it;
    }

    variable::Value simodo::setup::Setup::value(const std::string &id) const
    {
        const auto it = std::find_if(_description.setup.begin(), _description.setup.end(), 
                                    [id](const SetupStructure & s){
                                        return s.id == id;
                                    });

        return it == _description.setup.end() ? variable::Value {} : it->value;
    }

    void simodo::setup::Setup::setValue(const std::string & /*id*/, const variable::Value & /*value*/)
    {
    }

}