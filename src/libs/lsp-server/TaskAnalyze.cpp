#include "TaskAnalyze.h"
#include "simodo/lsp-server/DocumentContext.h"

namespace simodo::lsp
{

TaskAnalyze::TaskAnalyze(DocumentContext & doc) 
    : _doc(doc)
{
}

void TaskAnalyze::work()
{
    _doc.analyze();
}

}