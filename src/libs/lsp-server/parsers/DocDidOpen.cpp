#include "DocDidOpen.h"
#include "simodo/lsp-server/ServerContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"

namespace simodo::lsp
{

void DocDidOpen::work()
{
    if (_jsonrpc.is_valid() && _jsonrpc.params().type() == variable::ValueType::Object) {
        if (_server.openDocument(*_jsonrpc.params().getObject()))
            return;
    }
    _server.log().error("There are wrong parameter structure of 'textDocument/didOpen' notification", variable::toJson(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::JsonRpc(-1,u"There are wrong parameter structure of 'textDocument/didOpen' notification",-1));
}

}