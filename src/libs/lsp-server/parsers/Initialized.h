#ifndef TaskInitialized_h
#define TaskInitialized_h

#include "simodo/tp/Task_interface.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/Variable.h"

namespace simodo::lsp
{

class ServerContext;

class Initialized : public tp::Task_interface
{
    ServerContext &     _server;
    variable::JsonRpc _jsonrpc;

public:
    Initialized() = delete;
    Initialized(ServerContext & server, std::string jsonrpc)
        : _server(server), _jsonrpc(jsonrpc)
    {}

    virtual void work() override;
};

}

#endif // TaskInitialized_h