#include "DocDidClose.h"
#include "simodo/lsp-server/ServerContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"
#include "simodo/inout/convert/functions.h"

namespace simodo::lsp
{

void DocDidClose::work()
{
    if (_jsonrpc.is_valid() && _jsonrpc.params().type() == variable::ValueType::Object) {
        variable::Value & textDocument_value = _jsonrpc.params().getObject()->find(u"textDocument");
        if (textDocument_value.type() == variable::ValueType::Object) {
            variable::Value & uri_value = textDocument_value.getObject()->find(u"uri");
            if (uri_value.type() == variable::ValueType::String) {
                if (_server.closeDocument(inout::toU8(uri_value.getString())))
                    return;
            }
        }
    }
    _server.log().error("There are wrong parameter structure of 'textDocument/didClose' notification", variable::toJson(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::JsonRpc(-1,u"There are wrong parameter structure of 'textDocument/didClose' notification",-1));
}

}