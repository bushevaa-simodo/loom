#include "SegmentationFault.h"
#include "simodo/lsp-server/ServerContext.h"
#include "simodo/lsp-server/DocumentContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"

namespace simodo::lsp
{

void SegmentationFault::work()
{
    if (_jsonrpc.is_valid()) {
        lsp::TextDocumentPositionParams doc_position;
        DocumentContext *               doc = _server.findDocument(doc_position.textDocument.uri);
        variable::Value                 result = doc->produceHoverResponse(doc_position.position);
        
        _server.sending().push(variable::JsonRpc(result, _jsonrpc.id()));
        return;
    }
    _server.log().error("There are wrong parameter structure of 'SegmentationFault' command", 
                        variable::toJson(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::JsonRpc(-1,u"There are wrong parameter structure of 'SegmentationFault' command",
                            _jsonrpc.id()));
}

}