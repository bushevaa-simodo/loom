/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/token/TokenQualification.h"


namespace simodo::inout
{
    const char * getQualificationName(TokenQualification qua)
    {
        switch(qua)
        {
        case TokenQualification::None:
            return "None";
        case TokenQualification::Keyword:
            return "Keyword";
        case TokenQualification::Integer:
            return "Integer";
        case TokenQualification::RealNumber:
            return "RealNumber";
        case TokenQualification::NewLine:
            return "NewLine";
        case TokenQualification::NationalCharacterUse:
            return "NationalCharacterUse";
        case TokenQualification::NationalCharacterMix:
            return "NationalCharacterMix";
        case TokenQualification::UnknownCharacterSet:
            return "UnknownCharacterSet";
        case TokenQualification::NotANumber:
            return "NotANumber";
        default:
            return "*****";
        }
    }

}