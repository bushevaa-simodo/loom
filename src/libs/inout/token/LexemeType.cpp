/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/token/LexemeType.h"


namespace simodo::inout
{
    const char * getLexemeTypeName(LexemeType type)
    {
        switch(type)
        {
        case LexemeType::Compound:
            return "Compound";
        case LexemeType::Empty:
            return "Empty";
        case LexemeType::Punctuation:
            return "Punctuation";
        case LexemeType::Id:
            // Значение проверяется в Highlighter::highlightElement
            return "Word";
        case LexemeType::Annotation:
            return "Annotation";
        case LexemeType::Number:
            return "Number";
        case LexemeType::Comment:
            return "Comment";
        case LexemeType::Error:
            return "Error";
        case LexemeType::NewLine:
            return "NewLine";
        default:
            return "*****";
        }
    }

}