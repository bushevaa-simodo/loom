#include "simodo/inout/format/fmt.h"

namespace simodo::inout
{
    void fmt::replace(const char * s, size_t size)
    {
        if (_shift > 10*2)
            throw std::overflow_error("The number of formatting arguments exceeds the allowed number");

        if (size == std::string::npos)
            size = std::strlen(s);

        std::string::size_type pos = 0;

        while(true) {
            pos = _str.find(FMT_PATTERN+_shift, pos, 2);

            if (pos == std::string::npos)
                break;

            _str.replace(pos,2,s);
            pos += size;
        }
    }
}
