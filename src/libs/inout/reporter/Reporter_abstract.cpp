/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/reporter/Reporter_abstract.h"

#include "simodo/inout/convert/functions.h"

namespace simodo::inout
{
    void Reporter_abstract::reportWithPosition(const SeverityLevel level, const Location &location, const std::string &briefly, const std::string &atlarge)
    {
        report( level,
                location,
                briefly,
                atlarge.empty() ? ("Позиция разбора: " + getLocationString(location)) : atlarge);
    }

    const char * getSeverityLevelName(const SeverityLevel level)
    {
        switch(level)
        {
        case SeverityLevel::Information:
            return "";
        case SeverityLevel::Warning:
            return "Предупреждение: ";
        case SeverityLevel::Error:
            return "Ошибка: ";
        case SeverityLevel::Fatal:
            return "Сбой! ";
        }
        return "";
    }

    std::string getLocationString(const Location &location, bool in_detail)
    {
        /// \todo Нужно убрать лишнее преобразование кодировок
        std::string str = location.uri()
                + ":" + std::to_string(static_cast<unsigned>(location.range().start().line()+1))
                + ":" + std::to_string(static_cast<unsigned>(location.range().start().character()+1));

        if (in_detail)
            str += "[" + std::to_string(location.range().end().line()+1)
                    + "," + std::to_string(location.range().end().character()+1)
                    + "]";

        return str;
    }

}