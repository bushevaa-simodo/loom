/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/reporter/StreamReporter.h"

#include "simodo/inout/convert/functions.h"

#include <iostream>

namespace simodo::inout
{
    void StreamReporter::report(const SeverityLevel level, const Location &, const std::string &briefly, const std::string &atlarge)
    {
        _out << getSeverityLevelName(level) << briefly << std::endl;
        if (!atlarge.empty())
            _out << atlarge << std::endl;
    }

}