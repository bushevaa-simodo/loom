/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/reporter/ConsoleReporter.h"

#include "simodo/inout/convert/functions.h"

#include <iostream>
#include <mutex>

namespace simodo::inout
{
    void ConsoleReporter::report(const SeverityLevel level, const Location &, const std::string &briefly, const std::string &atlarge)
    {
        static std::mutex console_output_mutex;

        if (level >= _max_severity_level)
        {
            std::lock_guard locker(console_output_mutex);

            std::cout << getSeverityLevelName(level) << briefly << std::endl;
            if (!atlarge.empty())
                std::cout << atlarge << std::endl;
        }

        _max_report_level = std::max(_max_severity_level,level);
    }

}