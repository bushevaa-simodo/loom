/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/variable/Variable.h"

std::string simodo::variable::getValueTypeName(ValueType type) noexcept
{
    std::string s;

    switch(type)
    {
    case ValueType::Null:
        s = "null";
        break;
    case ValueType::Bool:
        s = "bool";
        break;
    case ValueType::Int:
        s = "int";
        break;
    case ValueType::Float:
        s = "float";
        break;
    case ValueType::String:
        s = "string";
        break;
    case ValueType::Function:
        s = "function";
        break;
    case ValueType::Object:
        s = "object";
        break;
    case ValueType::Array:
        s = "array";
        break;
    case ValueType::Ref:
        s = "ref";
        break;
    case ValueType::ExtFunction:
        s = "function (external)";
        break;
    case ValueType::IntFunction:
        s = "function (internal)";
        break;
    default:
        s = UNDEF_STRING;
        break;
    }

    return s;
}
