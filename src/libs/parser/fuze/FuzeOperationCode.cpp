/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/parser/fuze/FuzeOperationCode.h"

namespace simodo::parser
{
    const char * getOperationCodeName(FuzeOperationCode op) noexcept
    {
        switch(op)
        {
        case FuzeOperationCode::None:
            return "None";
        case FuzeOperationCode::Main:
            return "Main";
        case FuzeOperationCode::GlobalScript:
            return "GlobalScript";
        case FuzeOperationCode::Production:
            return "Production";
        case FuzeOperationCode::Production_Pattern:
            return "Production_Pattern";
        case FuzeOperationCode::Production_Direction:
            return "Production_Direction";
        case FuzeOperationCode::Production_Script:
            return "Production_Script";
        default:
            return "***";
        }
    }

}