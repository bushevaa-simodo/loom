#include "simodo/loom/FiberStatus.h"

namespace simodo::loom
{
    const char * getFiberStatusName(FiberStatus status)
    {
        switch(status)
        {
        case FiberStatus::Queued:
            return "Queued";
        
        case FiberStatus::Flow:
            return "Flow";
        
        case FiberStatus::Complete:
            return "Complete";
        
        case FiberStatus::Delayed:
            return "Delayed";
        
        case FiberStatus::Paused:
            return "Paused";
        }

        return "?";
    }

}