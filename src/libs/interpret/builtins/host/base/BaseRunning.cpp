/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/interpret/builtins/hosts/base/BaseRunning.h"

namespace simodo::interpret::builtins
{

    BaseRunning::BaseRunning(Interpret * inter)
        : BaseInterpret_abstract(inter)
    {

    }

}