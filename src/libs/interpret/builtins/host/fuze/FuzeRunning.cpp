/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/interpret/builtins/hosts/fuze/FuzeRunning.h"

namespace simodo::interpret::builtins
{

FuzeRunning::FuzeRunning(Interpret * inter, 
                         std::string grammar_name,
                         parser::Grammar & grammar,
                         parser::TableBuildMethod method,
                         bool need_strict_rule_consistency)
    : FuzeInterpret_abstract(inter, grammar_name, grammar, method, need_strict_rule_consistency)
{

}

}