#include "simodo/lsp-client/CompletionParams.h"

#include "simodo/inout/convert/functions.h"

namespace simodo::lsp
{

bool parseCompletionContext(const simodo::variable::Value & context_value, CompletionContext & context)
{
    if (context_value.type() != variable::ValueType::Object)
        return false;

    const std::shared_ptr<variable::Object> context_object = context_value.getObject();

    const variable::Value & triggerKind_value         = context_object->find(u"triggerKind");
    const variable::Value & triggerCharacter_value    = context_object->find(u"triggerCharacter");

    if (triggerKind_value.type() != variable::ValueType::Int
     || triggerCharacter_value.type() != variable::ValueType::String)
        return false;

    context.triggerKind      = static_cast<CompletionTriggerKind>(triggerKind_value.getInt());
    context.triggerCharacter = inout::toU8(triggerCharacter_value.getString());

    return true;
}

bool parseCompletionParams(const simodo::variable::Value & params, CompletionParams & completionParams)
{
    if (params.type() != variable::ValueType::Object
     || !parseTextDocumentPositionParams(params, completionParams))
        return false;

    const std::shared_ptr<variable::Object> params_object = params.getObject();

    return parseCompletionContext(params_object->find(u"context"), completionParams.context);
}

}
