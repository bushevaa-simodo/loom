#include "simodo/lsp-client/LspEnums.h"

namespace simodo::lsp
{

std::u16string getDiagnosticSeverityString(DiagnosticSeverity severity)
{
    switch(severity)
    {
    case DiagnosticSeverity::Error:
        return u"error";
    case DiagnosticSeverity::Warning:
        return u"warning";
    case DiagnosticSeverity::Information:
        return u"information";
    case DiagnosticSeverity::Hint:
        return u"hint";
    }
    return u"*";
}

std::u16string getSymbolKindString(SymbolKind kind)
{
    switch(kind)
    {	
    case SymbolKind::File: 
        return u"File";
	case SymbolKind::Module:      
        return u"Module";
	case SymbolKind::Namespace:   
        return u"Namespace";
	case SymbolKind::Package:     
        return u"Package";
	case SymbolKind::Class:       
        return u"Class";
	case SymbolKind::Method:      
        return u"Method";
	case SymbolKind::Property:    
        return u"Property";
	case SymbolKind::Field:       
        return u"Field";
	case SymbolKind::Constructor: 
        return u"Constructor";
	case SymbolKind::Enum:        
        return u"Enum";
	case SymbolKind::Interface:   
        return u"Interface";
	case SymbolKind::Function:    
        return u"Function";
	case SymbolKind::Variable:    
        return u"Variable";
	case SymbolKind::Constant:    
        return u"Constant";
	case SymbolKind::String:      
        return u"String";
	case SymbolKind::Number:      
        return u"Number";
	case SymbolKind::Boolean:     
        return u"Boolean";
	case SymbolKind::Array:       
        return u"Array";
	case SymbolKind::Object:      
        return u"Object";
	case SymbolKind::Key:         
        return u"Key";
	case SymbolKind::Null:        
        return u"Null";
	case SymbolKind::EnumMember:  
        return u"EnumMember";
	case SymbolKind::Struct:      
        return u"Struct";
	case SymbolKind::Event:       
        return u"Event";
	case SymbolKind::Operator:    
        return u"Operator";
	case SymbolKind::TypeParameter:
        return u"TypeParameter";
    }
    return u"*";
}

}
