/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/ast/Node.h"

namespace simodo::ast 
{
    void Node::swap(Node & node)
    {
        _branches.swap(node._branches);

        std::u16string  host        = node._host;
        OperationCode   operation   = node._operation;
        inout::Token    token       = node._token;
        inout::Token    bound       = node._bound;
       
        node._host      = _host;
        node._operation = _operation;
        node._token     = _token;
        node._bound     = _bound;

        _host       = host;
        _operation  = operation;
        _token      = token;
        _bound      = bound;
    }

}
