/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

/*! \file Утилита тестирования средств лексического анализа библиотеки SIMODO core. Проект SIMODO.
*/

#include "simodo/module/HardModuleLoader.h"
#include "simodo/inout/convert/functions.h"

#include <iostream>

using namespace simodo;

int main(int argc, char *argv[])
{
    std::string module_name {"test"};

    if (argc > 1)
        module_name = argv[1];

    module::HardModuleLoader loader({"bin/modules"});

    std::shared_ptr<variable::Module_interface> module_object = loader.load(module_name);
    if (!module_object) {
        std::cout << "Unable to create module '" << module_name << "'" << std::endl;
        return 1;
    }

    variable::Object record = module_object->instantiate(module_object);
    variable::Value  value(record);
    
    std::cout << module_name << ": " << inout::toU8(toString(value)) << std::endl;
    return 0;
}
