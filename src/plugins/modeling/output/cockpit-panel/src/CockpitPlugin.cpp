#include "CockpitPlugin.h"
#include "CockpitPanelAdaptor.h"

using namespace simodo;

CockpitPlugin::CockpitPlugin()
{
}

shell::PanelAdaptor_interface * CockpitPlugin::createPanelAdaptor(shell::Access_interface & shell_access)
{
    return new CockpitPanelAdaptor(shell_access);
}

shell::service_t CockpitPlugin::supports() const
{
    return shell::PS_Pan_Singleton
            // | shell::Find
            // | shell::Replace
    ;
}
