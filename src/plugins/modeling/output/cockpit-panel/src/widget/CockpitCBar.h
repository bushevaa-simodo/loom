#ifndef COCKPITCBAR_H
#define COCKPITCBAR_H

#include "CockpitBar.h"

class CockpitCBar : public CockpitBar
{
public:
    enum class ValueRisingDirection
    {
        Clockwise,
        Counterclockwise
    };

private:
    static constexpr qreal SIDE_PADDING_MULTIPLIER = 1.0 / 20;
    static const QString TEXT_FONT_FAMILY;

    ValueRisingDirection _value_rising_direction;

public:
    CockpitCBar(ValueRisingDirection value_rising_direction = ValueRisingDirection::Clockwise);

    void paintEvent(QPaintEvent * event) override;
};

#endif // COCKPITHBAR_H
