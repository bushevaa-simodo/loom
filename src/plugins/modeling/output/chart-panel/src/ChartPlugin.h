#ifndef ChartPlugin_H
#define ChartPlugin_H

#include "simodo/shell/panel/Panel_plugin.h"

class ChartPlugin: public QObject, public simodo::shell::Panel_plugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "BMSTU.SIMODO.shell.plugin.Panel" FILE "ChartPlugin.json")
    Q_INTERFACES(simodo::shell::Panel_plugin)

public:
    ChartPlugin();

public:
    simodo::version_t version() const override;
    QString id() const override;
    virtual QString designation() const override { return "Chart"; }
    simodo::shell::service_t supports() const override;
    QSet<QString> workWithDocuments() const override;

public:
    simodo::shell::PanelAdaptor_interface *createPanelAdaptor(
        simodo::shell::Access_interface &plug_data
    ) override;
    Qt::DockWidgetAreas allowed_areas() const override;
    Qt::DockWidgetArea attach_to() const override;

};

#endif // ChartPlugin_H
