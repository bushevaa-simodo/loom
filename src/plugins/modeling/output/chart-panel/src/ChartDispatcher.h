#ifndef ChartDispatcher_H
#define ChartDispatcher_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/AdaptorModeling_interface.h"
#include "simodo/shell/access/Access_interface.h"

#include "domain/ChartController.h"
#include "domain/ChartView.h"

#include <QtCore>
#include <QtCharts>

#include <functional>
#include <optional>

namespace shell  = simodo::shell;

class ChartDispatcher : public QObject
                      , public shell::AdaptorModeling_interface
{
    Q_OBJECT

public:
    using ControllerFactory = std::function<
        ChartController *(QString)
    >;

    ChartDispatcher(
        ControllerFactory controller_factory
        , QObject *parent = nullptr
    );
    ~ChartDispatcher();

public:
    // shell::AdaptorModeling_interface
    void started() override;
    void paused() override;
    void stoped() override;
    bool acceptData(const QString &func, QVector<QString> &data) override;

signals:
    void initRequested(QString title);

private:
    ControllerFactory _controller_factory;
    QHash<QString, QPointer<ChartController>> _controllers;

    bool _resendData(
        const QString controller_name
        , const QString &func
        , QVector<QString> &data
    );
    bool _multiresendData(const QString &func, QVector<QString> &data);
};

#endif // ChartDispatcher_H
