#ifndef MechanoidPanelAdaptor_H
#define MechanoidPanelAdaptor_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/AdaptorModeling_interface.h"
#include "simodo/shell/access/Access_interface.h"

#include <QtCore>

namespace shell  = simodo::shell;

class MechanoidPanelAdaptor : public QObject,
                          public shell::PanelAdaptor_interface,
                          public shell::AdaptorModeling_interface
{
    shell::Access_interface & _shell;

    QString     _title;

public:
    MechanoidPanelAdaptor(shell::Access_interface & shell);
    ~MechanoidPanelAdaptor();

public:
    // shell::PanelAdaptor_interface
    virtual QWidget * panel_widget() override;
    virtual QString title() const override;
    virtual void reset() override {}
    virtual void changedCurrentDocument(const QString & ) override {}
    virtual bool activateService(shell::service_t ) override { return false; }
    virtual bool acceptData(const QJsonObject & ) override { return false; }
    virtual shell::AdaptorModeling_interface * getModelingInterface() override { return this; }

public:
    // shell::AdaptorModeling_interface
    virtual void started() override;
    virtual void paused() override;
    virtual void stoped() override;
    virtual bool acceptData(const QString & func, QVector<QString> & data) override;

private:
};

#endif // MechanoidPanelAdaptor_H
