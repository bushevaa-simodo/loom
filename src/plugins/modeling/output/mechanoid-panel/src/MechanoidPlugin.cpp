#include "MechanoidPlugin.h"
#include "MechanoidPanelAdaptor.h"

using namespace simodo;

MechanoidPlugin::MechanoidPlugin()
{
}

shell::PanelAdaptor_interface * MechanoidPlugin::createPanelAdaptor(shell::Access_interface & shell_access)
{
    return new MechanoidPanelAdaptor(shell_access);
}

shell::service_t MechanoidPlugin::supports() const
{
    return shell::PS_Pan_Singleton
    ;
}
