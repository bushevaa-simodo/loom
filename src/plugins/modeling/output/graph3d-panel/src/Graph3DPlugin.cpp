#include "Graph3DPlugin.h"
#include "Graph3DPanelAdaptor.h"

using namespace simodo;

Graph3DPlugin::Graph3DPlugin()
{
}

shell::PanelAdaptor_interface * Graph3DPlugin::createPanelAdaptor(shell::Access_interface & shell_access)
{
    return new Graph3DPanelAdaptor(shell_access);
}

shell::service_t Graph3DPlugin::supports() const
{
    return shell::PS_None
        // | shell::PS_Pan_Singleton
    ;
}
