#include "Graph3DPanelAdaptor.h"

Graph3DPanelAdaptor::Graph3DPanelAdaptor(shell::Access_interface & shell)
    : _shell(shell)
{
    _graph3d = new Graph3D();
    _widget = QWidget::createWindowContainer(_graph3d);
    _widget->installEventFilter(this);
}

Graph3DPanelAdaptor::~Graph3DPanelAdaptor()
{
}

QString Graph3DPanelAdaptor::title() const
{
    return _title;
}

void Graph3DPanelAdaptor::started() 
{
}

void Graph3DPanelAdaptor::paused() 
{
}

void Graph3DPanelAdaptor::stoped()
{
}

bool Graph3DPanelAdaptor::acceptData(const QString & func, QVector<QString> & data)
{
    if (func == "init" || func == "panel") {
        if (data.size() == 0)
            return false;

        _title = data.last();

        _shell.setPanelTitle("Graph3D", _title);
        _shell.risePanel("Graph3D", _title);
        _graph3d->handleInitialization(_title);
        return true;
    }

    if (func == "addSeries") {
        if (data.size() == 0 || data.size() % 2 != 0)
            return false;

        for (auto it = data.constBegin(); it != data.constEnd(); it += 2)
        {
            _graph3d->handleAddSeries(*it, (it + 1)->toInt());
        }
        return true;
    }

    if (func == "addPoint") {
        if (data.size() == 0 || data.size() % 4 != 0)
            return false;

        for (auto it = data.constBegin(); it < data.constEnd(); it += 4)
        {
            _graph3d->handleAddPoint(*it, (it + 1)->toDouble(), (it + 2)->toDouble(), (it + 3)->toDouble());
        }
        return true;
    }

    if (func == "setPointsCount") {
        if (data.size() == 0 || data.size() % 2 != 0)
            return false;

        for (auto it = data.constBegin(); it != data.constEnd(); it += 2)
        {
            _graph3d->handleSetPointsCount(*it, (it + 1)->toInt());
        }
        return true;
    }

    return false;
}

bool Graph3DPanelAdaptor::eventFilter(QObject *, QEvent *event)
{
    if (event->type() == QEvent::PaletteChange) {
        _graph3d->resetTheme();
    }

    return false;
}
