#ifndef GRAPH3D_H
#define GRAPH3D_H

#include <QDockWidget>
#include <QMenu>
#include <Q3DScatter>

class Graph3D : public QtDataVisualization::Q3DScatter
{
    Q_OBJECT

    QtDataVisualization::Q3DTheme * _theme_light;
    QtDataVisualization::Q3DTheme * _theme_dark;

    std::map<QString,int> _series_point_counts;

public:
    Graph3D();

    void handleInitialization(const QString & title);
    QtDataVisualization::QScatter3DSeries * handleAddSeries(const QString & series_name, int mesh);
    void handleAddPoint(const QString & series_name, float x, float y, float z);
    void handleSetPointsCount(const QString & series_name, int count);

    void resetTheme();
};

#endif // GRAPH3D_H
