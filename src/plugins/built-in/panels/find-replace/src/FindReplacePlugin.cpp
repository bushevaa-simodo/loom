#include "FindReplacePlugin.h"
#include "PanelAdaptor.h"

using namespace simodo;

FindReplacePlugin::FindReplacePlugin()
{
}

shell::PanelAdaptor_interface * FindReplacePlugin::createPanelAdaptor(shell::Access_interface & shell_access)
{
    return new PanelAdaptor(shell_access);
}

shell::service_t FindReplacePlugin::supports() const
{
    return shell::PS_Pan_Singleton
         | shell::PS_Pan_Find
         | shell::PS_Pan_Replace
    ;
}
