#ifndef HomeDirectorySortProxy_H
#define HomeDirectorySortProxy_H

#include <QSortFilterProxyModel>

class HomeDirectorySortProxy : public QSortFilterProxyModel
{
    Q_OBJECT

    QStringList _hidden_names;
    QString _search_name;

public:
    HomeDirectorySortProxy(QStringList hidden_names);

    virtual bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

    void setSearchName(const QString & search_name) { _search_name = search_name; }
    void doInvalidateFilter() { invalidateFilter(); }
};


#endif // HomeDirectorySortProxy_H
