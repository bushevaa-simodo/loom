#include "HomeDirectoryAdaptor.h"

#include <QDir>

HomeDirectoryAdaptor::HomeDirectoryAdaptor(shell::Access_interface & plug_data)
    : _shell(plug_data)
{
    _widget = new HomeDirectoryWidget(this, plug_data); 
}

QString HomeDirectoryAdaptor::title() const
{
    return QDir::currentPath().split(QDir::separator()).back();
}

void HomeDirectoryAdaptor::reset()
{
    _widget->resetWorkDirectory();
}

void HomeDirectoryAdaptor::changedCurrentDocument(const QString & path)
{
    if (!path.isEmpty())
        _widget->setSelectorPosition(path);
}

