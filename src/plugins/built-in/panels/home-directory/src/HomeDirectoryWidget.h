#ifndef HomeDirectoryWidget_H
#define HomeDirectoryWidget_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/Access_interface.h"

QT_BEGIN_NAMESPACE
class QFileSystemModel;
class QToolBar;
class QLineEdit;
QT_END_NAMESPACE

namespace shell = simodo::shell;

class HomeDirectoryTree;

enum class FileEditChoice
{
    NewFile, NewDirectory, Find, NewFileHome, NewDirectoryHome
};

class HomeDirectoryWidget : public QWidget
{
    Q_OBJECT

    shell::PanelAdaptor_interface * _adaptor;
    shell::Access_interface &       _shell_access;

    HomeDirectoryTree *             _tree;
    QToolBar *                      _toolbar;
    QLineEdit *                     _file_name_editor;

    QAction *       _act_open_file;
    QAction *       _act_open_file_in_associated_program;
    QAction *       _act_new_file;
    QAction *       _act_new_directory;
    QAction *       _act_root_new_file;
    QAction *       _act_root_new_directory;
    QAction *       _act_delete_file;
    QAction *       _act_duplicate_file;
    QAction *       _act_rename_file;
    /// \todo Поиск работает плохо, пока выключаем
    // QAction *       _act_find_item;

    QAction *       _act_collapse_all;
    QAction *       _act_expand_all;
    // QAction *       _act_refresh;

    FileEditChoice  _choice;

public:
    HomeDirectoryWidget(shell::PanelAdaptor_interface *adaptor, shell::Access_interface & plug_data);
    virtual ~HomeDirectoryWidget() override;

    const FileEditChoice &  
                choice() { return _choice; }

    QLineEdit *     fileNameEditor()  { return _file_name_editor; }
    bool            setSelectorPosition(const QString & path);
    void            resetWorkDirectory();

protected:
    void            fillToolbar();
    void            createActions();

    virtual void    contextMenuEvent(QContextMenuEvent *event) override;
};

#endif // WorkDirectoryWidget_H
