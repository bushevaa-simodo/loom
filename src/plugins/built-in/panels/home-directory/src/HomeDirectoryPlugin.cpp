#include "HomeDirectoryPlugin.h"
#include "HomeDirectoryAdaptor.h"

using namespace simodo;

HomeDirectoryPlugin::HomeDirectoryPlugin()
{
}

shell::PanelAdaptor_interface * HomeDirectoryPlugin::createPanelAdaptor(shell::Access_interface & plug_data)
{
    return new HomeDirectoryAdaptor(plug_data);
}

unsigned HomeDirectoryPlugin::supports() const
{
    return shell::PS_Pan_Singleton
         | shell::PS_Pan_ShowOnStart;
}
