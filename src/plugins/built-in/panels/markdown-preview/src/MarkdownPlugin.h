#ifndef MarkdownPlugin_H
#define MarkdownPlugin_H

#include "simodo/shell/panel/Panel_plugin.h"

#include <QSet>

namespace shell  = simodo::shell;

class MarkdownPlugin: public QObject, public shell::Panel_plugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "BMSTU.SIMODO.shell.plugin.Panel" FILE "MarkdownPlugin.json")
    Q_INTERFACES(simodo::shell::Panel_plugin)

public:
    MarkdownPlugin();

    virtual simodo::version_t version() const override { return shell::version(); }
    virtual shell::PanelAdaptor_interface * createPanelAdaptor(shell::Access_interface & plug_data) override;

    virtual QString id() const override { return "markdown-preview"; }
    virtual QString designation() const override { return "Preview"; }
    virtual Qt::DockWidgetAreas allowed_areas() const override { return Qt::AllDockWidgetAreas; }
    virtual Qt::DockWidgetArea attach_to() const override { return Qt::RightDockWidgetArea; }

    virtual shell::service_t supports() const override;

    virtual QSet<QString> workWithDocuments() const override { return {}; }
};

#endif // MarkdownPlugin_H
