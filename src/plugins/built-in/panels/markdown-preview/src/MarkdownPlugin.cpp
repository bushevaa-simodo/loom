#include "MarkdownPlugin.h"
#include "MarkdownAdaptor.h"

using namespace simodo;

MarkdownPlugin::MarkdownPlugin()
{
}

shell::PanelAdaptor_interface * MarkdownPlugin::createPanelAdaptor(shell::Access_interface & shell_access)
{
    return new MarkdownAdaptor(shell_access);
}

shell::service_t MarkdownPlugin::supports() const
{
    return shell::PS_Pan_Singleton
    ;
}
