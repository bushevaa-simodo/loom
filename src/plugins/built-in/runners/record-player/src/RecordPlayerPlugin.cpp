#include "RecordPlayerPlugin.h"
#include "RecordPlayer.h"

using namespace simodo;

RecordPlayerPlugin::RecordPlayerPlugin()
{
}

shell::Runner_interface * RecordPlayerPlugin::createRunner(shell::Access_interface & shell_access)
{
    return new RecordPlayer(this, shell_access);
}
