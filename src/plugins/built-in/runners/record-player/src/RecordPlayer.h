#ifndef RecordPlayer_H
#define RecordPlayer_H

#include "simodo/shell/runner/Runner_interface.h"
#include "simodo/shell/runner/Runner_plugin.h"
#include "simodo/shell/access/RunnerManagement_interface.h"

#include <QObject>

#include <thread>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <atomic>

inline QString SIMULATION_RECORD_EXTENTION {".simodo-values"};

namespace shell  = simodo::shell;

class RecordPlayer : public QObject,
                     public shell::Runner_interface
{
    Q_OBJECT

    shell::Runner_plugin *      _plugin;
    shell::Access_interface &   _shell;
    shell::RunnerManagement_interface * 
                                _run_management = nullptr;

    shell::RunnerState          _state = shell::RunnerState::Stoped;

    QString                         _ran_path;
    std::unique_ptr<std::thread>    _thread;
    std::condition_variable         _pause_condition;
    std::mutex                      _pause_mutex;
    std::atomic_bool                _lets_stop;
    std::atomic_bool                _lets_pause;
    QString                         _error_text;

    void thread_function();

public:
    RecordPlayer(shell::Runner_plugin * plugin, shell::Access_interface & shell);

public:
    // shell::Runner_interface
    virtual shell::Runner_plugin * plugin() override { return _plugin; }
    virtual QString name() const override;
    virtual QString description() const override;
    virtual bool isRunnable(const QString & path) override;
    virtual bool startModeling(const QString & path) override;
    virtual bool pauseModeling() override;
    virtual bool stopModeling() override;
    virtual bool send(const QString &) override { return true; }
    virtual QString error_text() override { return _error_text; }
    virtual QWidget * getToolbarWidget() override { return nullptr; }
    virtual bool isAutoRunActive() const override { return false; }
    virtual void setAutoRunActive(bool ) override {}

private slots:
    void notification_received_slot(const QString text);
    void ran_slot();
    void paused_slot();
    void stoped_slot();

signals:
    void ran_signal();
    void stoped_signal();
    void paused_signal();
    void notification_received_signal(const QString text);
};

#endif // RecordPlayer_H
