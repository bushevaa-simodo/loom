#ifndef RecordPlayerPlugin_H
#define RecordPlayerPlugin_H

#include "simodo/shell/runner/Runner_plugin.h"
#include "simodo/shell/runner/Runner_interface.h"

#include <QObject>
#include <QSet>

namespace shell  = simodo::shell;

class RecordPlayerPlugin: public QObject, 
                          public shell::Runner_plugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "BMSTU.SIMODO.shell.plugin.Runner" FILE "RecordPlayerPlugin.json")
    Q_INTERFACES(simodo::shell::Runner_plugin)

public:
    RecordPlayerPlugin();

public:
    // shell::Plugin_interface
    virtual simodo::version_t version() const override { return shell::version(); }
    virtual shell::service_t supports() const override { return shell::PS_None; }
    virtual QString id() const override { return "RecordPlayer"; }
    // shell::Runner_plugin
    virtual shell::Runner_interface * createRunner(shell::Access_interface & plug_data) override;
};

#endif // RecordPlayerPlugin_H
