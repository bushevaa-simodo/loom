#include "LspSupportPlugin.h"
#include "LspSupport.h"

using namespace simodo;

LspSupportPlugin::LspSupportPlugin()
{
}

shell::Runner_interface * LspSupportPlugin::createRunner(shell::Access_interface & shell_access)
{
    return new LspSupport(this, shell_access);
}
