#ifndef LspSupportPlugin_H
#define LspSupportPlugin_H

#include "simodo/shell/runner/Runner_plugin.h"
#include "simodo/shell/runner/Runner_interface.h"

#include <QObject>
#include <QSet>

namespace shell  = simodo::shell;

class LspSupportPlugin: public QObject, 
                        public shell::Runner_plugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "BMSTU.SIMODO.shell.plugin.Runner" FILE "LspSupportPlugin.json")
    Q_INTERFACES(simodo::shell::Runner_plugin)

public:
    LspSupportPlugin();

public:
    // shell::Plugin_interface
    virtual simodo::version_t version() const override { return shell::version(); }
    virtual shell::service_t supports() const override { return shell::PS_None | shell::PS_Run_AutoPreview; }
    virtual QString id() const override { return "LspSupport"; }
    // shell::Runner_plugin
    virtual shell::Runner_interface * createRunner(shell::Access_interface & plug_data) override;
};

#endif // LspSupportPlugin_H
