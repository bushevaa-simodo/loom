#ifndef StarsInterpreterPlugin_H
#define StarsInterpreterPlugin_H

#include "simodo/shell/runner/Runner_plugin.h"
#include "simodo/shell/runner/Runner_interface.h"

#include <QObject>
#include <QSet>

namespace shell  = simodo::shell;

class StarsInterpreterPlugin: public QObject, 
                          public shell::Runner_plugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "BMSTU.SIMODO.shell.plugin.Runner" FILE "StarsInterpreterPlugin.json")
    Q_INTERFACES(simodo::shell::Runner_plugin)

public:
    StarsInterpreterPlugin();

public:
    // shell::Runner_plugin
    virtual simodo::version_t version() const override { return shell::version(); }
    virtual shell::service_t supports() const override { return shell::PS_None; }
    virtual QString id() const override { return "StarsInterpreter"; }
    virtual shell::Runner_interface * createRunner(shell::Access_interface & plug_data) override;
};

#endif // StarsInterpreterPlugin_H
