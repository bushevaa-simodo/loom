#ifndef SIMODO_DSL_SERVER_CallbackTypedMsgHandler
#define SIMODO_DSL_SERVER_CallbackTypedMsgHandler

#include "TypedMsgHandler.h"

namespace simodo::edit::plugin::model_server
{
    class CallbackTypedMsgHandler : public TypedMsgHandler
    {
    public:
        using Callback = std::function<bool(QJsonObject)>;

        CallbackTypedMsgHandler(QString type, Callback callback);
        virtual ~CallbackTypedMsgHandler() = default;

        bool handle(QJsonObject msg) override;

    private:
        Callback _callback;
    };
} // simodo::dsl::server

#endif // SIMODO_DSL_SERVER_CallbackTypedMsgHandler
