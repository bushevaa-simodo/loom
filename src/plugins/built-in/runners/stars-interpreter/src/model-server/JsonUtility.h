#ifndef JsonUtility_H
#define JsonUtility_H

#include <optional>
#include <QtCore>

namespace simodo::edit::plugin::model_server::JsonUtility
{
    std::optional<double> findNumber(QJsonObject & o, QString key);
    std::optional<QString> findString(QJsonObject & o, QString key);
} // simodo::edit::plugin::model_server::JsonUtility

#endif // JsonUtiltiy_H
