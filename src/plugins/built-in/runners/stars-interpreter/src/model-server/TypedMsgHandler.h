#ifndef TypedMsgHandler_H
#define TypedMsgHandler_H

#include "MsgHandler.h"

namespace simodo::edit::plugin::model_server
{
    class TypedMsgHandler : public MsgHandler
    {
    public:
        TypedMsgHandler(QString type)
            : _type(type)
        {}
        virtual ~TypedMsgHandler() = default;

        bool handle(QJsonObject msg) override;

    protected:
        QString _type;
    };
} // simodo::edit::plugin::model_server

#endif // TypedMsgHandler_H
