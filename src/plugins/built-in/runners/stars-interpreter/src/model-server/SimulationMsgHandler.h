#ifndef SimulationMsgHandler_H
#define SimulationMsgHandler_H

#include "TypedMsgHandler.h"

#include "simodo/shell/access/Access_interface.h"

namespace simodo::edit::plugin::model_server
{
    class SimulationMsgHandler : public TypedMsgHandler
    {
    public:
        using WriteCallback = std::function<void(QString, simodo::shell::MessageSeverity)>;

    public:
        SimulationMsgHandler(
            WriteCallback write_callback
        )
            : TypedMsgHandler("message")
            , _write_callback(write_callback)
        {}
        ~SimulationMsgHandler() override = default;

        bool handle(QJsonObject msg) override;

    private:
        WriteCallback _write_callback;
    };
} // simodo::edit::plugin::model_server

#endif // SimulationMsgHandler_H
