#include "RequestMsgHandler.h"

#include "JsonUtility.h"

#include "simodo/shell/document/DocumentAdaptor_interface.h"

using namespace simodo::edit::plugin::model_server;

bool RequestMsgHandler::handle(QJsonObject msg)
{
    if (!TypedMsgHandler::handle(msg))
    {
        return false;
    }

    auto path = JsonUtility::findString(msg, "path");

    QJsonObject module_jo;
    module_jo.insert("type", "module");
    module_jo.insert("path", *path);

    if (!path)
    {
        auto error_str = "Server: Request: Path: Missed";
        _access.sendGlobal(error_str);
        module_jo.insert("error", error_str);
    }
    else
    {
        auto adaptor = _access.getDocumentAdaptor(*path, true);
        if (adaptor == nullptr)
        {
            module_jo.insert("error", "Could not open " + (*path));
        }
        else
        {
            module_jo.insert("script", adaptor->text());
        }
    }

    _response_callback(module_jo);
    return true;
}
