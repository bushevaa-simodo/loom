#ifndef RequestMsgHandler_H
#define RequestMsgHandler_H

#include "TypedMsgHandler.h"

#include "simodo/shell/access/Access_interface.h"

namespace simodo::edit::plugin::model_server
{
    class RequestMsgHandler : public TypedMsgHandler
    {
    public:
        using ResponseCallback = std::function<void(QJsonObject)>;

        RequestMsgHandler( simodo::shell::Access_interface & access
                           , ResponseCallback response_callback
                           )
            : TypedMsgHandler("request")
            , _access(access)
            , _response_callback(response_callback)
        {}
        ~RequestMsgHandler() override = default;

        bool handle(QJsonObject msg) override;

    private:
        simodo::shell::Access_interface & _access;
        ResponseCallback _response_callback;
    };
} // simodo::edit::plugin::model_server

#endif // RequestMsgHandler_H
