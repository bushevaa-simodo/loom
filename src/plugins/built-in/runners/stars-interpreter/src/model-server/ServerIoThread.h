#ifndef ServerIoThread_H
#define ServerIoThread_H

#include "ServerIoService.h"

#include <thread>

namespace simodo::edit::plugin::model_server
{

class ServerIoThread
{
public:
    ServerIoThread(std::string process_path, std::vector<std::string> process_args);
    ~ServerIoThread();

    void start();
    bool isRunning();
    void stop();

    ServerIoService & service()
    {
        return _service;
    }

private:
    boost::asio::thread_pool _pool;
    boost::asio::io_service _io;
    std::shared_ptr<boost::asio::io_service::work> _io_work;
    ServerIoService _service;
};

}

#endif // ServerIoThread_H
