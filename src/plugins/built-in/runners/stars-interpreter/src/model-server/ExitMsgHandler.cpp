#include "ExitMsgHandler.h"

#include "JsonUtility.h"

using namespace simodo::edit::plugin::model_server;

bool ExitMsgHandler::handle(QJsonObject msg)
{
    if (!TypedMsgHandler::handle(msg))
    {
        return false;
    }

    _exit_callback();
    return true;
}
