#ifndef ResultMsgHandler_H
#define ResultMsgHandler_H

#include "TypedMsgHandler.h"

#include "simodo/shell/access/Access_interface.h"

namespace simodo::edit::plugin::model_server
{
    class ResultMsgHandler : public TypedMsgHandler
    {
    public:
        ResultMsgHandler(simodo::shell::Access_interface & access)
            : TypedMsgHandler("result")
            , _access(access)
        {}
        ~ResultMsgHandler() override = default;

        bool handle(QJsonObject msg) override;

    private:
        simodo::shell::Access_interface & _access;
    };
} // simodo::edit::plugin::model_server

#endif // ResultMsgHandler_H
