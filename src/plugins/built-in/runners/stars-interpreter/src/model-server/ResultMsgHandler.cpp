#include "ResultMsgHandler.h"

#include "JsonUtility.h"

using namespace simodo::edit::plugin::model_server;

bool ResultMsgHandler::handle(QJsonObject msg)
{
    if (!TypedMsgHandler::handle(msg))
    {
        return false;
    }

    auto code = JsonUtility::findNumber(msg, "code");
    if (!code)
    {
        _access.sendGlobal("Server: Result: Code: Missed", shell::MessageSeverity::Warning);
        return true;
    }

    _access.sendGlobal("Server: Result: " + QString::number(int(*code)), shell::MessageSeverity::Debug);
    return true;
}
