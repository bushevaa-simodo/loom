#include "TypedMsgHandler.h"

#include "JsonUtility.h"

using namespace simodo::edit::plugin::model_server;

bool TypedMsgHandler::handle(QJsonObject msg)
{
    if (JsonUtility::findString(msg, "type") != _type)
    {
        return false;
    }

    return true;
}