#ifndef BufferedJsonIoService_H
#define BufferedJsonIoService_H

#include "JsonIoService.h"

class BufferedJsonIoService : public QObject
{
public:
    // 1 / 60Hz = 0.01(6)s
    static constexpr int DEFAULT_TIMER_INTERVAL = 100;

    BufferedJsonIoService(
        int timer_interval = DEFAULT_TIMER_INTERVAL
    );
    ~BufferedJsonIoService();

    void setReciever(QObject * reciever);

    void start(QString program, QStringList program_arguments);
    bool isRunning();
    void stop();

    void write(QJsonDocument message);

protected:
    bool event(QEvent * event) override;
    void timerEvent(QTimerEvent * event) override;

private:
    Q_OBJECT

    JsonIoService _service;
    /// @todo атомизировать работу с переменной
    QObject * _reciever;

    QQueue<QJsonDocument> _media_buffer;
    struct Timer
    {
        int id;
        int interval;
    } _timer;

    bool _messageEvent(JsonIoService::MessageEvent * event);

    void _startTimer();
    void _stopTimer();
};

#endif // BufferedJsonIoService_H