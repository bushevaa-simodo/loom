#include "DocumentPreviewPlugin.h"
#include "DocumentPreview.h"

using namespace simodo;

DocumentPreviewPlugin::DocumentPreviewPlugin()
{
}

shell::Runner_interface * DocumentPreviewPlugin::createRunner(shell::Access_interface & shell_access)
{
    return new DocumentPreview(this, shell_access);
}

shell::service_t DocumentPreviewPlugin::supports() const 
{ 
    return shell::PS_None
         | shell::PS_Run_AutoPreview
        //  | shell::PS_Run_AutoPreviewDef
         ; 
}

