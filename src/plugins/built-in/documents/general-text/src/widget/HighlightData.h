#ifndef HighlightData_H
#define HighlightData_H

#include "simodo/variable/Variable.h"

// #include <map>

struct HighlightData
{
    std::vector<std::shared_ptr<simodo::variable::Object>> tokenizers;
    std::vector<std::pair<std::shared_ptr<simodo::variable::Object>,std::u16string>> tokenizers_spec;
    std::shared_ptr<simodo::variable::Object> light     = nullptr;
    std::shared_ptr<simodo::variable::Object> dark      = nullptr;
};

#endif // HighlightData_H
