#ifndef LINENUMBERAREA_H
#define LINENUMBERAREA_H

#include <QWidget>

class CodeEdit;

class LineNumberArea : public QWidget
{
    CodeEdit * _editor;

public:
    LineNumberArea(CodeEdit *editor);

    virtual QSize sizeHint() const override;

protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual bool event(QEvent *event) override;
};

#endif // LINENUMBERAREA_H
