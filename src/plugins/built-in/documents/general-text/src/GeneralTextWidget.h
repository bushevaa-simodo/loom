#ifndef GeneralTextWidget_H
#define GeneralTextWidget_H

#include "simodo/shell/access/Access_interface.h"
#include "simodo/shell/access/LspStructures.h"

#include <QWidget>
#include <QStandardItemModel>

QT_BEGIN_NAMESPACE
class QComboBox;
class QTextDocument;
QT_END_NAMESPACE

namespace shell = simodo::shell;

class GeneralTextViewAdaptor;
class CodeEdit;

class GeneralTextWidget : public QWidget
{
    Q_OBJECT

    GeneralTextViewAdaptor &        _view;

    QComboBox *                     _combo;
    CodeEdit *                      _edit;

    QStandardItemModel              _completer_model;
    uint64_t                        _control_sum = 0;

    int     _cursor_position_change_timer  = 0;
    int     _last_line          = -1;
    int     _last_character     = -1;
    QString _last_left_punctuation;
    QString _need_completion_trigger;

public:
    GeneralTextWidget(GeneralTextViewAdaptor & view, QTextDocument * document);

    QComboBox * symbols_combo() { return _combo; }
    CodeEdit *  code_edit()     { return _edit; }
    shell::Access_interface & shell();

    void readyToWork();
    void nearToClose();

protected:
    virtual void timerEvent(QTimerEvent *event) override;
    virtual void focusInEvent(QFocusEvent * event) override;

protected:
    virtual bool eventFilter(QObject *obj, QEvent *event) override;

private:
    void checkDocSymbolChange(int line, int character);
};

#endif // GeneralTextWidget_H