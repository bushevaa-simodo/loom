#ifndef ImageViewerPlugin_H
#define ImageViewerPlugin_H

#include "simodo/shell/document/Document_plugin.h"

namespace shell = simodo::shell;

class ImageViewerPlugin: public QObject, public shell::Document_plugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "BMSTU.SIMODO.shell.plugin.Document" FILE "ImageViewerPlugin.json")
    Q_INTERFACES(simodo::shell::Document_plugin)

    QVector<QString> _extensions;

public:
    ImageViewerPlugin();

    virtual simodo::version_t version() const override { return shell::version(); }
    virtual QVector<QString> file_extensions() const override { return _extensions; }
    virtual QString view_name() const override;
    virtual QString view_short_name() const override;
    virtual QString id() const override;
    virtual QSet<QString> alternate_view_mnemonics(const QString & extention) const override;
    virtual std::shared_ptr<shell::DocumentAdaptor_interface> createDocumentAdaptor(shell::Access_interface & shell_access) override;
    virtual shell::ViewAdaptor_interface * createViewAdaptor(shell::Access_interface & shell_access,
                                            std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor,
                                            const QString & path_to_file) override;
    virtual shell::service_t supports() const override;
    virtual QString icon_theme() const override;
    virtual QIcon icon() const override { return QIcon(); }
};

#endif // ImageViewerPlugin_H