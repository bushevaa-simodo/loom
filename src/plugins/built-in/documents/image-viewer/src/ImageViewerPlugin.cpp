#include "ImageViewerPlugin.h"
#include "ViewAdaptor.h"

#include "QImageReader"

ImageViewerPlugin::ImageViewerPlugin()
{
    QList<QByteArray> type_list = QImageReader::supportedImageFormats();

    for(const auto & ba : type_list)
        _extensions << ba;
}

QString ImageViewerPlugin::view_name() const
{
    return tr("Image document");
}

QString ImageViewerPlugin::view_short_name() const
{
    return "image";
}

QString ImageViewerPlugin::id() const
{
    return "image";
}

QSet<QString> ImageViewerPlugin::alternate_view_mnemonics(const QString & ) const
{
    return {};
}

std::shared_ptr<shell::DocumentAdaptor_interface> ImageViewerPlugin::createDocumentAdaptor(shell::Access_interface & )
{
    return std::make_shared<ImageDocumentAdaptor>();
}

shell::ViewAdaptor_interface * ImageViewerPlugin::createViewAdaptor(shell::Access_interface & shell_access,
                                                        std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor,
                                                        const QString & )
{
    return new ImageViewAdaptor(shell_access, this, document_adaptor);
}

unsigned ImageViewerPlugin::supports() const
{
    return shell::PS_None
         | shell::PS_Doc_Open
         | shell::PS_Doc_Print
         | shell::PS_Doc_ExportToPdf
         | shell::PS_Doc_Export
         | shell::PS_Doc_Copy
         | shell::PS_Doc_Zoom
         | shell::PS_Doc_ReadOnly
         | shell::PS_Doc_Loadable
         ;
}

QString ImageViewerPlugin::icon_theme() const
{
    return "image-png";
}
