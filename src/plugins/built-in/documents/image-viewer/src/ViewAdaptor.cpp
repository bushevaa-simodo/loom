#include "ViewAdaptor.h"
#include "ImageViewerPlugin.h"

#include <QtPrintSupport/QtPrintSupport>

ImageViewAdaptor::ImageViewAdaptor(shell::Access_interface & shell_access, ImageViewerPlugin * plugin,
                        std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor)
    : _shell_access(shell_access)
    , _plugin(plugin)
    , _document_adaptor(document_adaptor)
{
    if(!document_adaptor) 
        _document_adaptor = plugin->createDocumentAdaptor(_shell_access);

    _viewer = new ImageViewerWidget(this, shell_access);
}

shell::Document_plugin * ImageViewAdaptor::plugin()
{
    return _plugin;
}

QString ImageViewAdaptor::loadFile(const QString & file_name)
{
    ImageDocumentAdaptor * document_adaptor = dynamic_cast<ImageDocumentAdaptor *>(_document_adaptor.get());

    QString error_string = _document_adaptor->loadFile(file_name);
    if (error_string.isEmpty() && document_adaptor) {
        _viewer->setImage(document_adaptor->image());
    }
    return error_string;
}

void ImageViewAdaptor::print(QPrinter * printer)
{
    _viewer->print(printer);
}

void ImageViewAdaptor::copyFromDocument()
{
    _viewer->copyImage();
}

void ImageViewAdaptor::zoomIn()
{
    _viewer->scaleImage(1.25);
}

void ImageViewAdaptor::zoomOut()
{
    _viewer->scaleImage(0.8);
}

void ImageViewAdaptor::zoomZero()
{
    _viewer->scaleImage(NAN);
}
