#ifndef ViewAdaptor_H
#define ViewAdaptor_H

#include "simodo/shell/document/ViewAdaptor_interface.h"
#include "simodo/shell/document/Document_plugin.h"

#include "DocumentAdaptor.h"
#include "ImageViewerWidget.h"

#include <QtPrintSupport/QtPrintSupport>

class ImageViewerPlugin;
class ImageDocumentAdaptor;

namespace shell = simodo::shell;

class ImageViewAdaptor : public QObject, public shell::ViewAdaptor_interface
{
    Q_OBJECT

    shell::Access_interface &   _shell_access;
    ImageViewerPlugin *         _plugin;
    std::shared_ptr<shell::DocumentAdaptor_interface>
                                _document_adaptor;
    ImageViewerWidget *         _viewer;

public:
    ImageViewAdaptor(shell::Access_interface & shell_access, ImageViewerPlugin * plugin,
                std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor);

    virtual std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor() override { return _document_adaptor; }
    virtual QWidget * document_widget() override { return _viewer; }
    virtual shell::Document_plugin * plugin() override;

    virtual void readyToWork(QWidget * ) override {}

    virtual QWidget * copyWidget() override { return nullptr; }

    virtual QString loadFile(const QString &file_name) override;
    virtual void setReadOnly() override {}
    virtual void print(QPrinter * printer) override;

    virtual void cutFromDocument() override {}
    virtual void copyFromDocument() override;
    virtual void pasteToDocument() override {}
    virtual bool hasSelection() override { return true; }
    virtual bool hasUndo() override { return false; }
    virtual bool hasRedo() override { return false; }

    virtual void zoomIn() override;
    virtual void zoomOut() override;
    virtual void zoomZero() override;

    virtual QPair<int,int> getLineCol() const override { return {0,0}; }
    virtual void setLineCol(QPair<int,int> ) override {}

    virtual bool acceptData(const QJsonObject & ) override { return false; }
    // virtual void find(const QString & ) override {}
    virtual shell::ViewAdaptorLsp_interface * getLspInterface() override { return nullptr; }
    virtual shell::ViewAdaptorFind_interface * getFindInterface() override { return nullptr; }
    virtual shell::AdaptorModeling_interface * getModelingInterface() override { return nullptr; }
    virtual void nearToClose() override {}
};

#endif // ViewAdaptor_H
