#ifndef ImageViewerWidget_H
#define ImageViewerWidget_H

#include "simodo/shell/access/Access_interface.h"

#include <QScrollArea>

class ImageDocumentAdaptor;
class ImageViewAdaptor;
class QLabel;
class QScrollBar;
class QPrinter;

namespace shell = simodo::shell;

class ImageViewerWidget : public QScrollArea
{
    Q_OBJECT

    ImageViewAdaptor *          _view_adaptor;
    shell::Access_interface &   _shell_access;
    ImageDocumentAdaptor *      _document_adaptor;

    QLabel *    _image_label;
    double      _scale_factor = 1;

public:
    ImageViewerWidget(ImageViewAdaptor * view_adaptor, shell::Access_interface & shell_access);

    void setImage(QImage &newImage);
    void print(QPrinter * printer);
    void copyImage();
    void scaleImage(double factor);

protected:
    virtual void focusInEvent(QFocusEvent * event) override;
    virtual void wheelEvent(QWheelEvent *event) override;

    void adjustScrollBar(QScrollBar *scrollBar, double factor);
};

#endif // ImageViewerWidget_H
