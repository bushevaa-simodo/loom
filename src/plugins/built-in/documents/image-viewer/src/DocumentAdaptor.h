#ifndef DocumentAdaptor_H
#define DocumentAdaptor_H

#include "simodo/shell/document/DocumentAdaptor_interface.h"

#include "QImage"

namespace shell = simodo::shell;

class ImageDocumentAdaptor : public shell::DocumentAdaptor_interface
{
    QImage _image;

public:
    ImageDocumentAdaptor();

    virtual QString loadFile(const QString & fileName) override;
    virtual QString saveFile(const QString &fileName) override;
    virtual bool wasSaved() override { return false; }
    virtual QString text() override;
    virtual bool setText(const QString &text) override;
    virtual bool isModified() override;
    virtual void setModified(bool modified) override;

    QImage & image() { return _image; }
};

#endif // DocumentAdaptor_H
