#include "DocumentAdaptor.h"

#include "QImageReader"

ImageDocumentAdaptor::ImageDocumentAdaptor()
{
}

QString ImageDocumentAdaptor::loadFile(const QString & fileName)
{
    QImageReader reader(fileName);
    reader.setAutoTransform(true);
    const QImage new_image = reader.read();
    if (new_image.isNull()) 
        return reader.errorString();
    
    _image = new_image;
    return "";
}

QString ImageDocumentAdaptor::saveFile(const QString &)
{
    return QObject::tr("Unsupported");
}

QString ImageDocumentAdaptor::text()
{
    /// @todo Не работает прямое преобразование, текст SVG нужно читать из файла
    // return QString::fromLocal8Bit(QByteArray(reinterpret_cast<char *>(_image.bits()), _image.sizeInBytes()));
    return "";
}

bool ImageDocumentAdaptor::setText(const QString &)
{
    return false;
}

bool ImageDocumentAdaptor::isModified()
{
    return false;
}

void ImageDocumentAdaptor::setModified(bool )
{
}
