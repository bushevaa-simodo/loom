#include "SetupViewAdaptor.h"
#include "SetupPlugin.h"

#include <QtPrintSupport/QtPrintSupport>

SetupViewAdaptor::SetupViewAdaptor(shell::Access_interface & shell_access, SetupPlugin * plugin,
                        std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor)
    : _shell_access(shell_access)
    , _plugin(plugin)
    , _document_adaptor(document_adaptor)
{
    if(!document_adaptor) 
        _document_adaptor = plugin->createDocumentAdaptor(_shell_access);

    _viewer = new SetupWidget();
}

shell::Document_plugin * SetupViewAdaptor::plugin()
{
    return _plugin;
}

QString SetupViewAdaptor::loadFile(const QString & )
{
    // ImageDocumentAdaptor * document_adaptor = dynamic_cast<ImageDocumentAdaptor *>(_document_adaptor.get());

    // QString error_string = _document_adaptor->loadFile(file_name);
    // if (error_string.isEmpty() && document_adaptor) {
    //     _viewer->setImage(document_adaptor->image());
    // }
    return "";
}

void SetupViewAdaptor::print(QPrinter * /*printer*/)
{
    // _viewer->print(printer);
}

void SetupViewAdaptor::copyFromDocument()
{
    // _viewer->copyImage();
}

void SetupViewAdaptor::zoomIn()
{
    // _viewer->scaleImage(1.25);
}

void SetupViewAdaptor::zoomOut()
{
    // _viewer->scaleImage(0.8);
}

void SetupViewAdaptor::zoomZero()
{
    // _viewer->scaleImage(NAN);
}
