#ifndef SetupDocumentAdaptor_H
#define SetupDocumentAdaptor_H

#include "simodo/shell/document/DocumentAdaptor_interface.h"

#include "QImage"

namespace shell = simodo::shell;

class SetupDocumentAdaptor : public shell::DocumentAdaptor_interface
{
    QImage _image;

public:
    SetupDocumentAdaptor();

    virtual QString loadFile(const QString & fileName) override;
    virtual QString saveFile(const QString &fileName) override;
    virtual bool wasSaved() override { return false; }
    virtual QString text() override;
    virtual bool setText(const QString &text) override;
    virtual bool isModified() override;
    virtual void setModified(bool modified) override;

    QImage & image() { return _image; }
};

#endif // SetupDocumentAdaptor_H
