#include "SetupPlugin.h"
#include "SetupViewAdaptor.h"

#include "QImageReader"

SetupPlugin::SetupPlugin()
{
}

QString SetupPlugin::view_name() const
{
    return tr("Image document");
}

QString SetupPlugin::view_short_name() const
{
    return "setup";
}

QString SetupPlugin::id() const
{
    return "setup";
}

QSet<QString> SetupPlugin::alternate_view_mnemonics(const QString & ) const
{
    return {};
}

std::shared_ptr<shell::DocumentAdaptor_interface> SetupPlugin::createDocumentAdaptor(shell::Access_interface & )
{
    return std::make_shared<SetupDocumentAdaptor>();
}

shell::ViewAdaptor_interface * SetupPlugin::createViewAdaptor(shell::Access_interface & shell_access,
                                                        std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor,
                                                        const QString & )
{
    return new SetupViewAdaptor(shell_access, this, document_adaptor);
}

unsigned SetupPlugin::supports() const
{
    return shell::PS_None
            // | shell::PS_Doc_Open
            // | shell::PS_Doc_Print
            // | shell::PS_Doc_ExportToPdf
            // | shell::PS_Doc_Export
            // | shell::PS_Doc_Copy
            // | shell::PS_Doc_Zoom
            // | shell::PS_Doc_ReadOnly
            // | shell::PS_Doc_Loadable
            ;
}

QString SetupPlugin::icon_theme() const
{
    return "setup-png";
}
