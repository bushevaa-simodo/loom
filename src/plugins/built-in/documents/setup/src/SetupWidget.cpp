#include "SetupWidget.h"
#include <iostream>

SetupWidget::SetupWidget()
{
    
    // Создание главного окна
    // QVBoxLayout *layout = new QVBoxLayout(this);

    // Создание виджета с прокруткой
    QScrollArea *scrollArea = new QScrollArea(this);
    QWidget *scrollWidget = new QWidget(scrollArea);
    QVBoxLayout *layout = new QVBoxLayout(scrollWidget);


    std::vector<std::string> arguments = {"test/source/setup/edit-setup.simodo-setup"};

    if (arguments.empty()) {
        std::cout << "Не задано имя файла параметров" << std::endl;
        // return 1;
    }

    std::string work_file;
    if (arguments.size() > 1)
    {
        work_file = arguments[1];
    }
    simodo::setup::Setup setup(arguments[0], work_file);

    bool ok = setup.load();

    if (!ok) {
        std::cout << "Ошибка при загрузке: " << setup.error() << std::endl;
        // return 1;
    }

    // Создаем дополнительный контейнерный виджет для группы виджетов: brief_label и decsription_label
    QWidget *descriptionContainer = new QWidget(this);
    QVBoxLayout *descriptionContainerLayout = new QVBoxLayout(descriptionContainer);

    // label для brief
    QLabel *brief_label = new QLabel(this);
    brief_label->setText(QString::fromUtf8(setup.setup().brief.c_str()));
    // layout->addWidget(brief_label);
    descriptionContainerLayout->addWidget(brief_label);
    this->widgetsMap.insert("main_brief", brief_label);

    // label для description
    QLabel *description_label = new QLabel(this);
    description_label->setText(QString::fromUtf8(setup.setup().description.c_str()));
    // layout->addWidget(description_label);
    descriptionContainerLayout->addWidget(description_label);
    this->widgetsMap.insert("main_description", description_label);

    descriptionContainer->setStyleSheet("QWidget:hover { background-color: lightgray; }");
    layout->addWidget(descriptionContainer);

    for (const simodo::setup::SetupStructure & ss : setup.setup().setup) {
        // Создаем дополнительный контейнерный виджет для группы виджетов: id_label, name_label, 
        //                                                                 brief_label, decsription_label и value
        QWidget *contentContainer = new QWidget(this);
        QVBoxLayout *contentContainerLayout = new QVBoxLayout(contentContainer);

        // label для id
        QLabel *id_label = new QLabel(this);
        id_label->setText(QString::fromUtf8(ss.id.c_str() != std::string("") ? ss.id.c_str() : "''"));
        // layout->addWidget(id_label);
        contentContainerLayout->addWidget(id_label);
        this->widgetsMap.insert("id", id_label);

        // label для name
        QLabel *name_label = new QLabel(this);
        name_label->setText(QString::fromUtf8(ss.name.c_str() != std::string("") ? ss.name.c_str() : "''"));
        // layout->addWidget(name_label);
        contentContainerLayout->addWidget(name_label);
        this->widgetsMap.insert("name", name_label);

        // label для brief
        QLabel *brief_label = new QLabel(this);
        brief_label->setText(QString::fromUtf8(ss.brief.c_str() != std::string("") ? ss.brief.c_str() : "''"));
        // layout->addWidget(brief_label);
        contentContainerLayout->addWidget(brief_label);
        this->widgetsMap.insert("brief", brief_label);

        // label для description
        QLabel *description_label = new QLabel(this);
        description_label->setText(QString::fromUtf8(ss.description.c_str() != std::string("") ? ss.description.c_str() : "''"));
        // layout->addWidget(description_label);
        contentContainerLayout->addWidget(description_label);
        this->widgetsMap.insert("description", description_label);

        // виджет для value
        if (ss.value.isNumber()) {
            QLineEdit *lineEdit = new QLineEdit(this);

            // Установка режима ввода только чисел
            QRegExpValidator* validator = new QRegExpValidator(QRegExp("[+-]?\\d*\\.?\\d+"), lineEdit);
            lineEdit->setValidator(validator);
            lineEdit->setText(QString::number(ss.value.getInt())); // Преобразование числа в строку
            lineEdit->setFixedSize(250, 30); // Задаем ширину и высоту кнопки
            // layout->addWidget(lineEdit);
            contentContainerLayout->addWidget(lineEdit);
            this->widgetsMap.insert("value", lineEdit);
        }
        else if (ss.value.isBoolean()) {
            QComboBox *comboBox = new QComboBox(this);
            comboBox->addItem("true");
            comboBox->addItem("false");
            ss.value.getBool() == true ? comboBox->setCurrentIndex(0) : comboBox->setCurrentIndex(1);
            comboBox->setFixedSize(250, 30); // Задаем ширину и высоту кнопки
            // layout->addWidget(comboBox);
            contentContainerLayout->addWidget(comboBox);
            this->widgetsMap.insert("value", comboBox);
        }
        else if (ss.value.isArray()) {
            QComboBox *comboBox = new QComboBox(this);

            for (simodo::variable::Value & ss_value : ss.value.getArray()->values()) {
                comboBox->addItem(QString::fromUtf16(ss_value.getString().c_str()));
            }

            comboBox->setFixedSize(250, 30); // Задаем ширину и высоту кнопки
            // layout->addWidget(comboBox);
            contentContainerLayout->addWidget(comboBox);
            this->widgetsMap.insert("value", comboBox);
        }

        contentContainer->setStyleSheet("QWidget:hover { background-color: lightgray; }");
        layout->addWidget(contentContainer);
    }

    // Создаем дополнительный контейнерный виджет для кнопки: saveButton
    QWidget *saveButtonContainer = new QWidget(this);
    QVBoxLayout *saveButtonContainerLayout = new QVBoxLayout(saveButtonContainer);

    // Создание кнопки "Сохранить"
    QPushButton *saveButton = new QPushButton("Save", this);
    saveButton->setFixedSize(250, 30); // Задаем ширину и высоту кнопки
    // layout->addWidget(saveButton);
    saveButtonContainerLayout->addWidget(saveButton);

    saveButtonContainer->setStyleSheet("QWidget:hover { background-color: lightgray; }");
    layout->addWidget(saveButtonContainer);


    // Обработчик нажатия на кнопку "Сохранить"
    QObject::connect(saveButton, &QPushButton::clicked, [&wm = this->widgetsMap]() {
    /*
        QJsonObject settingsObj;
    
        for (const QString &key : wm.keys())
        {
            QWidget *widget = wm.value(key);

            // Добавление строчных значений в сохраненный JSON
            if (QLabel *label = qobject_cast<QLabel*>(widget))
            {
                settingsObj.insert(key, label->text());
            }

            // Добавление значения из массива в сохраненный JSON
            else if (QComboBox *comboBox = qobject_cast<QComboBox*>(widget))
            {
                QString selectedValue = comboBox->currentText();
                settingsObj.insert(key, selectedValue);
            }

            // Добавление числовых значений в сохраненный JSON
            else if (QLineEdit *lineEdit = qobject_cast<QLineEdit*>(widget))
            {
                settingsObj.insert(key, lineEdit->text());
            }
        }
    
        QJsonDocument settingsDoc(settingsObj);

        QFile file("src/plugins/built-in/documents/setup/files/saved_settings.json");
        if (file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            file.write(settingsDoc.toJson());
            file.close();
            qDebug() << "Настройки сохранены в файл saved_settings.json";
        }
        else
        {
            qDebug() << "Не удалось сохранить настройки в файл";
        }
        */
    });

    this->setLayout(layout);

    scrollWidget->setLayout(layout);
    // Устанавливаем виджет с прокруткой
    scrollArea->setWidget(scrollWidget);
    scrollArea->setWidgetResizable(true);

    // Устанавливаем основной виджет
    this->setLayout(new QVBoxLayout(this));
    this->layout()->addWidget(scrollArea);

    // createMainLayout();
    // createDescriptionSection();
    // createContentSections();
    // createSaveButtonSection();
    // connectSaveButton();
    // setLayoutAndScrollArea();
}

SetupWidget::~SetupWidget()
{
    // Удаление виджетов из widgetsMap
    for (auto it = widgetsMap.begin(); it != widgetsMap.end(); ++it)
    {
        delete it.value();
    }
}



// void SetupWidget::createMainLayout()
// {
//     mainLayout = new QVBoxLayout(this);
// }

// void SetupWidget::loadSetup()
// {
//     std::vector<std::string> arguments = {"test/source/setup/edit-setup.simodo-setup"};

//     if (arguments.empty()) {
//         std::cout << "Не задано имя файла параметров" << std::endl;
//         // return 1;
//     }

//     setup = simodo::setup::Setup(arguments[0], arguments.size() > 1 ? arguments[1] : "");

//     bool ok = setup.load();

//     if (!ok) {
//         std::cout << "Ошибка при загрузке: " << setup.error() << std::endl;
//         // return 1;
//     }
// }

// void SetupWidget::createDescriptionSection()
// {
//     // Создаем дополнительный контейнерный виджет для группы виджетов: brief_label и decsription_label
//     this->descriptionContainer = new QWidget(this);
//     QVBoxLayout *descriptionContainerLayout = new QVBoxLayout(descriptionContainer);

//     // label для brief
//     QLabel *brief_label = new QLabel(this);
//     brief_label->setText(QString::fromUtf8(setup.setup().brief.c_str()));
//     // layout->addWidget(brief_label);
//     descriptionContainerLayout->addWidget(brief_label);
//     this->widgetsMap.insert("main_brief", brief_label);

//     // label для description
//     QLabel *description_label = new QLabel(this);
//     description_label->setText(QString::fromUtf8(setup.setup().description.c_str()));
//     // layout->addWidget(description_label);
//     descriptionContainerLayout->addWidget(description_label);
//     this->widgetsMap.insert("main_description", description_label);

//     descriptionContainer->setStyleSheet("QWidget:hover { background-color: lightgray; }");
//     this->mainLayout->addWidget(this->descriptionContainer);
// }

// void SetupWidget::createContentSections()
// {
//     for (const simodo::setup::SetupStructure & ss : this->setup.setup().setup) {
//         // Создаем дополнительный контейнерный виджет для группы виджетов: id_label, name_label, 
//         //                                                                 brief_label, decsription_label и value
//         this->contentContainer = new QWidget(this);
//         QVBoxLayout *contentContainerLayout = new QVBoxLayout(contentContainer);

//         // label для id
//         QLabel *id_label = new QLabel(this);
//         id_label->setText(QString::fromUtf8(ss.id.c_str() != std::string("") ? ss.id.c_str() : "''"));
//         // layout->addWidget(id_label);
//         contentContainerLayout->addWidget(id_label);
//         this->widgetsMap.insert("id", id_label);

//         // label для name
//         QLabel *name_label = new QLabel(this);
//         name_label->setText(QString::fromUtf8(ss.name.c_str() != std::string("") ? ss.name.c_str() : "''"));
//         // layout->addWidget(name_label);
//         contentContainerLayout->addWidget(name_label);
//         this->widgetsMap.insert("name", name_label);

//         // label для brief
//         QLabel *brief_label = new QLabel(this);
//         brief_label->setText(QString::fromUtf8(ss.brief.c_str() != std::string("") ? ss.brief.c_str() : "''"));
//         // layout->addWidget(brief_label);
//         contentContainerLayout->addWidget(brief_label);
//         this->widgetsMap.insert("brief", brief_label);

//         // label для description
//         QLabel *description_label = new QLabel(this);
//         description_label->setText(QString::fromUtf8(ss.description.c_str() != std::string("") ? ss.description.c_str() : "''"));
//         // layout->addWidget(description_label);
//         contentContainerLayout->addWidget(description_label);
//         this->widgetsMap.insert("description", description_label);

//         // виджет для value
//         if (ss.value.isNumber()) {
//             QLineEdit *lineEdit = new QLineEdit(this);

//             // Установка режима ввода только чисел
//             QRegExpValidator* validator = new QRegExpValidator(QRegExp("[+-]?\\d*\\.?\\d+"), lineEdit);
//             lineEdit->setValidator(validator);
//             lineEdit->setText(QString::number(ss.value.getInt())); // Преобразование числа в строку
//             lineEdit->setFixedSize(250, 30); // Задаем ширину и высоту кнопки
//             // layout->addWidget(lineEdit);
//             contentContainerLayout->addWidget(lineEdit);
//             this->widgetsMap.insert("value", lineEdit);
//         }
//         else if (ss.value.isBoolean()) {
//             QComboBox *comboBox = new QComboBox(this);
//             comboBox->addItem("true");
//             comboBox->addItem("false");
//             ss.value.getBool() == true ? comboBox->setCurrentIndex(0) : comboBox->setCurrentIndex(1);
//             comboBox->setFixedSize(250, 30); // Задаем ширину и высоту кнопки
//             // layout->addWidget(comboBox);
//             contentContainerLayout->addWidget(comboBox);
//             this->widgetsMap.insert("value", comboBox);
//         }
//         else if (ss.value.isArray()) {
//             QComboBox *comboBox = new QComboBox(this);

//             for (simodo::variable::Value & ss_value : ss.value.getArray()->values()) {
//                 comboBox->addItem(QString::fromUtf16(ss_value.getString().c_str()));
//             }

//             comboBox->setFixedSize(250, 30); // Задаем ширину и высоту кнопки
//             // layout->addWidget(comboBox);
//             contentContainerLayout->addWidget(comboBox);
//             this->widgetsMap.insert("value", comboBox);
//         }

//         contentContainer->setStyleSheet("QWidget:hover { background-color: lightgray; }");
//         this->mainLayout->addWidget(this->contentContainer);
//     }
// }

// void SetupWidget::createSaveButtonSection()
// {
//     // Создаем дополнительный контейнерный виджет для кнопки: saveButton
//     this->saveButtonContainer = new QWidget(this);
//     QVBoxLayout *saveButtonContainerLayout = new QVBoxLayout(this->saveButtonContainer);

//     // Создание кнопки "Сохранить"
//     this->saveButton = new QPushButton("Save", this);
//     saveButton->setFixedSize(250, 30); // Задаем ширину и высоту кнопки
//     // layout->addWidget(saveButton);
//     saveButtonContainerLayout->addWidget(saveButton);

//     saveButtonContainer->setStyleSheet("QWidget:hover { background-color: lightgray; }");
//     this->mainLayout->addWidget(this->saveButtonContainer);
// }

// void SetupWidget::connectSaveButton()
// {
//     // Обработчик нажатия на кнопку "Сохранить"
//     connect(this->saveButton, &QPushButton::clicked, this, &SetupWidget::saveSettings);
// }

// void SetupWidget::saveSettings()
// {
//     /*
//     // Сохранение настроек в файл
//     QFile file("src/plugins/built-in/documents/setup/files/saved_settings.json");
//     if (file.open(QIODevice::WriteOnly | QIODevice::Text))
//     {
//         file.write(settingsDoc.toJson());
//         file.close();
//         qDebug() << "Settings saved to file saved_settings.json";
//     }
//     */
// }

// void SetupWidget::setLayoutAndScrollArea()
// {
//     // Установка основного layout и виджета с возможностью прокрутки
//     mainLayout->addWidget(saveButtonContainer);
//     scrollArea->setWidget(scrollWidget);
//     scrollArea->setWidgetResizable(true);
//     mainLayout->addWidget(scrollArea);
//     setLayout(mainLayout);
// }
