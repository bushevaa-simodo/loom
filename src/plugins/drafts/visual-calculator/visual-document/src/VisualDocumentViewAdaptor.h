#ifndef ViewAdaptor_H
#define ViewAdaptor_H

#include "Export.h"

#include "simodo/shell/document/ViewAdaptor_interface.h"
#include "simodo/shell/document/Document_plugin.h"

#include "VisualDocumentAdaptor.h"

#include <memory>

class VisualDocumentPlugin;

namespace QtNodes
{
class GraphicsView;
class DataFlowGraphicsScene;
}

namespace shell = simodo::shell;

class VISUAL_CALCULATOR_EXPORT VisualDocumentViewAdaptor : public QObject, public shell::ViewAdaptor_interface
{
    Q_OBJECT

    shell::Access_interface &           _shell_access;
    VisualDocumentPlugin *              _plugin;
    std::shared_ptr<shell::DocumentAdaptor_interface>  
                                        _document_adaptor;

    QtNodes::DataFlowGraphicsScene *    _scene;
    QtNodes::GraphicsView *             _editor;

public:
    VisualDocumentViewAdaptor(shell::Access_interface & shell_access,
                VisualDocumentPlugin * plugin,
                std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor);

    virtual std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor() override { return _document_adaptor; }
    virtual QWidget * document_widget() override;
    virtual shell::Document_plugin * plugin() override;

    virtual void readyToWork(QWidget * ) override {}

    virtual QWidget * copyWidget() override;

    virtual QString loadFile(const QString &file_name) override;
    virtual void setReadOnly() override;
    virtual void print(QPrinter * printer) override;

    virtual void cutFromDocument() override;
    virtual void copyFromDocument() override;
    virtual void pasteToDocument() override;
    virtual bool hasSelection() override;
    virtual bool hasUndo() override;
    virtual bool hasRedo() override;

    virtual void zoomIn() override;
    virtual void zoomOut() override;
    virtual void zoomZero() override;

    virtual QPair<int,int> getLineCol() const override;
    virtual void setLineCol(QPair<int,int> line_col) override;

    virtual bool acceptData(const QJsonObject & ) override { return false; }
    virtual shell::ViewAdaptorLsp_interface * getLspInterface() override { return nullptr; }
    virtual shell::ViewAdaptorFind_interface * getFindInterface() override { return nullptr; }
    virtual shell::AdaptorModeling_interface * getModelingInterface() override { return nullptr; }
    virtual void nearToClose() override {}

    void addNode(const QString & node_name);
};

#endif // ViewAdaptor_H
