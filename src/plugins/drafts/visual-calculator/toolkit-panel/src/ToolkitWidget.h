#ifndef ToolkitWidget_H
#define ToolkitWidget_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/Access_interface.h"

#include <QToolBox>
#include <QListWidgetItem>

namespace shell  = simodo::shell;

class ToolkitWidget : public QToolBox
{
    Q_OBJECT

    shell::PanelAdaptor_interface * _adaptor;
    shell::Access_interface &       _shell_access;

public:
    ToolkitWidget(shell::PanelAdaptor_interface *adaptor, shell::Access_interface & plug_data);
    virtual ~ToolkitWidget() override;

public slots:
    void itemDoubleClicked(QListWidgetItem *item);

protected:

};

#endif // ToolkitWidget_H
