#include "PanelAdaptor.h"

/// @todo Переделать на передачу активному документу JSON-структуры
#include <VisualDocumentViewAdaptor.h>

PanelAdaptor::PanelAdaptor(shell::Access_interface & shell_access)
    : _shell_access(shell_access)
{
    _widget = new ToolkitWidget(this, _shell_access);
}

QString PanelAdaptor::title() const
{
    return QObject::tr("VC Toolkit");
}

void PanelAdaptor::changedCurrentDocument(const QString & )
{
}

bool PanelAdaptor::activateService(shell::service_t )
{
    Q_ASSERT(_widget);

    return true;
}

void PanelAdaptor::addNode(const QString &node_name)
{
    shell::ViewAdaptor_interface * view_interface = const_cast<shell::ViewAdaptor_interface *>(_shell_access.getCurrentViewAdaptor());
    if (view_interface == nullptr)
        return;

    VisualDocumentViewAdaptor * document_view_adaptor = dynamic_cast<VisualDocumentViewAdaptor *>(view_interface);
    if (document_view_adaptor == nullptr)
        return;

    document_view_adaptor->addNode(node_name);
}
