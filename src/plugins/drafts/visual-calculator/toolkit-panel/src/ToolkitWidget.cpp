#include <QtWidgets>

#include "ToolkitWidget.h"
#include "PanelAdaptor.h"

ToolkitWidget::ToolkitWidget(shell::PanelAdaptor_interface *adaptor, shell::Access_interface &shell_access)
    : _adaptor(adaptor)
    , _shell_access(shell_access)
{
    QListWidget * source_result_nodes = new QListWidget;
    source_result_nodes->setViewMode(QListView::IconMode);

    source_result_nodes->addItem(new QListWidgetItem(QIcon(":/images/Chess.512"), "NumberSource"));
    source_result_nodes->addItem(new QListWidgetItem(QIcon(":/images/Grab.512"), "Result"));

    QListWidget * arithmetic_nodes = new QListWidget;
    arithmetic_nodes->setViewMode(QListView::IconMode);
//    arithmetic_nodes->setWrapping(true);
//    arithmetic_nodes->setFlow(QListView::LeftToRight);
//    arithmetic_nodes->setLayoutMode(QListView::Batched);
//    arithmetic_nodes->setUniformItemSizes(true);

    arithmetic_nodes->addItem(new QListWidgetItem(QIcon(":/images/KeyChainAccess.512"), "Addition"));
    arithmetic_nodes->addItem(new QListWidgetItem(QIcon(":/images/LaunchPad.512"), "Subtraction"));
    arithmetic_nodes->addItem(new QListWidgetItem(QIcon(":/images/Network.512"), "Multiplication"));
    arithmetic_nodes->addItem(new QListWidgetItem(QIcon(":/images/PhotosApp.512"), "Division"));

    addItem(source_result_nodes, QIcon(":/images/MissionControl.512"), tr("Sources/Results"));
    addItem(arithmetic_nodes, QIcon(":/images/Calculator.512"), tr("Arithmetic Nodes"));

    connect(source_result_nodes, &QListWidget::itemDoubleClicked, this, &ToolkitWidget::itemDoubleClicked);
    connect(arithmetic_nodes, &QListWidget::itemDoubleClicked, this, &ToolkitWidget::itemDoubleClicked);
}

ToolkitWidget::~ToolkitWidget()
{
}

void ToolkitWidget::itemDoubleClicked(QListWidgetItem *item)
{
    PanelAdaptor * panel_adaptor = dynamic_cast<PanelAdaptor *>(_adaptor);
    if (panel_adaptor == nullptr)
        return;

    panel_adaptor->addNode(item->text());
}
