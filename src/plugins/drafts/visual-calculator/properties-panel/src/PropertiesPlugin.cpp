#include "PropertiesPlugin.h"
#include "PanelAdaptor.h"

PropertiesPlugin::PropertiesPlugin()
{
}

shell::PanelAdaptor_interface * PropertiesPlugin::createPanelAdaptor(shell::Access_interface & shell_access)
{
    return new PanelAdaptor(shell_access);
}

shell::service_t PropertiesPlugin::supports() const
{
    return shell::PS_Pan_Singleton
            ;
}

QSet<QString> PropertiesPlugin::workWithDocuments() const
{
    return {"visual-calculator"};
}
