#include "PanelAdaptor.h"

PanelAdaptor::PanelAdaptor(shell::Access_interface & shell_access)
    : _shell_access(shell_access)
{
    _widget = new PropertiesWidget(this, _shell_access);
}

QString PanelAdaptor::title() const
{
    return QObject::tr("VC Properties");
}

void PanelAdaptor::changedCurrentDocument(const QString & )
{
}

bool PanelAdaptor::activateService(shell::service_t )
{
    Q_ASSERT(_widget);

    return true;
}

bool PanelAdaptor::acceptData(const QJsonObject & data)
{    
    return _widget->acceptData(data);
}

