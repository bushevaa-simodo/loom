#ifndef PanelAdaptor_H
#define PanelAdaptor_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"

#include "PropertiesWidget.h"

namespace shell  = simodo::shell;

class PanelAdaptor : public shell::PanelAdaptor_interface
{
    shell::Access_interface & _shell_access;
    PropertiesWidget * _widget;

public:
    PanelAdaptor(shell::Access_interface & shell_access);

    virtual QWidget * panel_widget() override { return _widget; }
    virtual QString title() const override;
    virtual void reset() override {}
    virtual void changedCurrentDocument(const QString & file_name) override;
    virtual bool activateService(shell::service_t service) override;
    virtual bool acceptData(const QJsonObject & data) override;
    virtual shell::AdaptorModeling_interface * getModelingInterface() override { return nullptr; }
};

#endif // PanelAdaptor_H
