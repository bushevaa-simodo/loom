/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_ast_generator_AstFormationFlow
#define simodo_ast_generator_AstFormationFlow

/*! \file FormationFlow.h
    \brief Класс построения АДОС в стиле перемещения указателя в формируемом потоке
*/

#include "simodo/ast/generator/FormationFlow_interface.h"

#include <string>
#include <vector>

namespace simodo::ast
{
    /*!
     * \brief Класс построения АДОС в стиле перемещения указателя в выходном потоке
     */
    class FormationFlow : public FormationFlow_interface
    {
        friend class FormationWrapper;

        Tree                 _tree; ///< Формируемое АДОС
        std::vector<Node *>  _flow; ///< Указатель на текущий узел, для которого выполняется построение ветки АДОС

    public:
        FormationFlow();

    public:
        virtual void            addNode(const std::u16string & host, 
                                        OperationCode op, 
                                        const inout::Token & op_symbol, 
                                        const inout::Token & bound) override;
        virtual void            addNode_StepInto(const std::u16string & host, 
                                        OperationCode op, 
                                        const inout::Token & op_symbol, 
                                        const inout::Token & bound) override;
        virtual bool            goParent() override;
        virtual void            addFile(const std::string & file_path) override;
        virtual const Tree &    tree() const override { return _tree; }
        virtual void            finalize() override {}
    };

}

#endif // simodo_ast_generator_AstFormationFlow
