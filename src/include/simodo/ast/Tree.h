/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_ast_Tree
#define simodo_ast_Tree

/*! \file Tree.h
    \brief Структуры абстрактного синтаксического дерева
*/

#include "simodo/ast/Node.h"


namespace simodo::ast 
{
    class Tree {
        // Классы, которые участвуют в формировании Node
        friend class FormationFlow;
        friend class FormationWrapper;

        inout::uri_set_t    _files;
        Node                _root;

    public:
        const inout::uri_set_t & files()    const { return _files; }
        const Node &             root()     const { return _root; }

    protected:
        inout::uri_set_t &       mutable_files()  { return _files; }
        Node &                   mutable_root()   { return _root; }
    };
}

#endif // simodo_ast_Node
