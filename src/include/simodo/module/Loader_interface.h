/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef SIMODO_module_Loader_interface
#define SIMODO_module_Loader_interface

/*! \file Loader_interface.h
    \brief Интерфейс управления модулями.
*/

#include "simodo/variable/Module_interface.h"

#include <string>

namespace simodo::interpret
{
    class Interpret_interface;
}

namespace simodo::module
{
    class Loader_interface
    {
    public:
        virtual ~Loader_interface() = default;

        virtual std::shared_ptr<variable::Module_interface> load(const std::string & module_name,
                                                interpret::Interpret_interface * interpret = nullptr) = 0;
        virtual std::string                                 last_error() const = 0;
    };

    class ModuleManagement_null : public Loader_interface
    {
    public:
        virtual std::shared_ptr<variable::Module_interface> load(const std::string & ,
                                                interpret::Interpret_interface *  = nullptr) override
        {
            return std::shared_ptr<variable::Module_interface>(); 
        }
        virtual std::string last_error() const override { return "Unsupported"; }
    };

}

#endif // SIMODO_module_Loader_interface
