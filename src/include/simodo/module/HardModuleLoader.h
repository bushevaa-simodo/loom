/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_support_HardModuleLoader
#define SIMODO_support_HardModuleLoader

/*! \file HardModuleLoader.h
    \brief Управление модулями.
*/

#include "simodo/module/Loader_interface.h"
#include "simodo/module/HardModule.h"

#include <vector>
#include <memory>
#include <functional>

namespace simodo::interpret
{
    class Interpret_interface;
}

namespace simodo::module
{
    class HardModuleLoader : public Loader_interface
    {
        std::vector<std::string>    _module_places;
        std::string                 _last_error;

    public:
        HardModuleLoader() = delete;

        HardModuleLoader(std::vector<std::string> module_places)
            : _module_places(module_places)
        {}

        virtual std::shared_ptr<variable::Module_interface> 
                                            load(const std::string & module_name,
                                                interpret::Interpret_interface * interpret = nullptr) override;
        virtual std::string                 last_error() const override { return _last_error; }

    protected:
        std::function<ExtModuleFactory_t>   loadPluginFactory(const std::string & path, 
                                                const std::string & alias_name);
    };

}

#endif // SIMODO_support_HardModuleLoader
