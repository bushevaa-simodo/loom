/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_module_ModuleCollector
#define SIMODO_module_ModuleCollector

/*! \file ModuleCollector.h
    \brief Управление модулями.
*/

#include "simodo/module/ModuleCollector_interface.h"

#include <vector>
#include <unordered_map>
#include <memory>

namespace simodo::module
{
    class ModuleCollector : public ModuleCollector_interface
    {
        std::vector<std::string>    _module_places;
        std::unordered_map<std::string,std::shared_ptr<variable::Module_interface>> 
                                    _modules;  ///< Набор загруженных модулей
        std::string                 _last_error;

    public:
        ModuleCollector() = delete;
        ModuleCollector(std::vector<std::string> module_places);

        virtual std::shared_ptr<variable::Array>  produceObjectsByMask(const std::string & module_mask) override;
        virtual std::shared_ptr<variable::Object> produceObject(const std::string & module_name,
                                                        interpret::Interpret_interface * interpret = nullptr) override;
        virtual variable::Value     invoke(variable::Object & object, const std::string & method_name, const variable::VariableSet_t & args) override;
        virtual std::string         last_error() const override { return _last_error; }
    };

}

#endif // SIMODO_module_ModuleCollector
