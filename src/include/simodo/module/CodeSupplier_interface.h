/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_module_CodeSupplier_interface
#define simodo_module_CodeSupplier_interface

/*! \file CodeSupplier_interface.h
    \brief Интерфейс генератора кода для интерпретации.
    
*/

#include "simodo/inout/token/TokenLocation.h"
#include "simodo/ast/Node.h"

#include <string>


namespace simodo::module
{
    /**
     * @brief Интерфейс генератора кода для интерпретации.
     * 
     */
    class CodeSupplier_interface
    {
    public:
        virtual ~CodeSupplier_interface() = default;

        virtual const ast::Node *   getCode(const std::string & module_name,
                                            const inout::uri_set_t & files) = 0;

        virtual const std::string & last_error() const = 0;
       
    };

}

#endif // simodo_module_CodeSupplier_interface
