/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_StreamReporter
#define simodo_token_StreamReporter

/*! \file ConsoleReporter.h
    \brief Потоковая реализация "посетителя" (шаблон проектирования) для получения информации из среды SIMODO
*/

#include "simodo/inout/reporter/Reporter_abstract.h" 

#include <string>
#include <ostream>

namespace simodo::inout
{
    /*!
     * \brief Потоковая реализация "посетителя" (шаблон проектирования) для получения информации из среды SIMODO
     */
    class StreamReporter: public Reporter_abstract
    {
        std::ostream & _out;

    public:
        StreamReporter(std::ostream & out)
            : _out(out)
        {}

        virtual void report(const SeverityLevel level,
                            const Location & ,
                            const std::string & briefly,
                            const std::string & atlarge) override;
    };

}

#endif // simodo_token_StreamReporter
