/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_parser_RegularInputStreamSupplier
#define simodo_parser_RegularInputStreamSupplier

/*! \file RegularInputStreamSupplier.h
    \brief Интерфейс поставщика входных потоков.
    
*/

#include "simodo/inout/token/InputStreamSupplier_interface.h"

#include <memory>

namespace simodo::inout
{
    /**
     * @brief Интерфейс поставщика входных потоков.
     * 
     */
    class RegularInputStreamSupplier : public InputStreamSupplier_interface
    {
    public:
        virtual std::shared_ptr<InputStream_interface> supply(const std::string & path) override;
    };

}

#endif // simodo_parser_RegularInputStreamSupplier
