/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_LexemeType
#define simodo_token_LexemeType

/*! \file LexemeType.h
    \brief Тип лексемы абстрактного языка.
*/

#include <string>

namespace simodo::inout
{
    //! Тип лексемы
    enum class LexemeType
    {
        Empty = 0,      ///< Лексема неопределена (используется для обозначения пустого потока, конца файла)
        Punctuation,    ///< Разделитель, пунктуация или ключевое слово (возможно уточнение TokenQualification::Keyword)
        Id,             ///< Идентификатор (возможно уточнение TokenQualification::NationalCharacterMix)
        Annotation,     ///< Аннотация, строка символов
        Number,         ///< Число (возможно уточнение TokenQualification::Integer, TokenQualification::RealNumber, TokenQualification::NotANumber)
        Comment,        ///< Комментарий
        Error,          ///< Ошибка лексики (возможно уточнение TokenQualification::UnknownCharacterSet, TokenQualification::NationalCharacterMix, TokenQualification::NationalCharacterUse)
        Compound,       ///< Нетерминальный символ ("нетерминал")
        NewLine,        ///< Получена новая строка
    };

    /*!
     * \brief Функция получения строкового наименования типа лексемы
     * \param type  Тип лексемы (тип LexemeType)
     * \return      Ссылка на наименование типа лексемы
     */
    const char * getLexemeTypeName(LexemeType type);
}

#endif // simodo_token_LexemeType
