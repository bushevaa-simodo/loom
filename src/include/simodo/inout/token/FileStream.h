/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_FileStream
#define simodo_token_FileStream

/*! \file FileStream.h
    \brief Интерфейс входного потока и его реализации
*/

#include "simodo/inout/token/InputStream.h"

#include <fstream>

namespace simodo::inout
{
    /*!
     * \brief Реализация входного потока из файла
     *
     * \todo Хорошее место, чтобы подсчитывать точное количество символов, а не байт!
     * Нужно бы это реализовать.
     */
    class FileStream: public InputStream
    {
        std::ifstream  _in;

    public:
        FileStream() = delete;  ///< Пустой конструктор не поддерживается!

        /*!
         * \brief Конструктор входного потока из файла
         * \param in    Ссылка на входной поток
         */
        FileStream(const std::string & path);
    };
}

#endif // simodo_token_FileStream
