/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_TokenLocation
#define simodo_token_TokenLocation

/*! \file TokenLocation.h
    \brief Позиция токена в тексте.
*/

#include "simodo/inout/token/Location.h"

#include <cstdint>
#include <string>
#include <vector>

namespace simodo::inout
{
    typedef uint16_t uri_index_t;  ///< Тип для контекстов
    typedef uint16_t context_index_t;  ///< Тип для контекстов
    typedef std::vector<std::string> uri_set_t;

    inline constexpr context_index_t   NO_TOKEN_CONTEXT_INDEX = UINT16_MAX; ///< Недопустимый индекс отложенного контекста, означающий его отсутствие

    /*!
     * \brief Структура, определяющая местоположение токена
     *
     * Содержит характеристики местоположения.
     * 
     * Структурный класс (не совершает никаких действий над хранимыми элементами).
     */
     
    class TokenLocation
    {
        Range           _range;
        uri_index_t     _uri_index;
    
    public:
        TokenLocation() = delete;
        TokenLocation(uri_index_t uri_index, Range range)
            : _range(range)
            , _uri_index(uri_index)
        {}

        const Range &   range() const { return _range; }
        uri_index_t     uri_index() const { return _uri_index; }

        Location        makeLocation(const uri_set_t & uri_set) const 
        {
            return { (uri_set.size() <= _uri_index ? "" : uri_set[_uri_index]), _range };
        }

        void            merge(const TokenLocation & tl) { _range.merge(tl.range()); }
    };

    inline const static TokenLocation null_token_location {0,{{0,0},{0,0}}};
}

#endif // simodo_token_TokenLocation
