#ifndef simodo_lsp_LspData_H
#define simodo_lsp_LspData_H

#include <string>

namespace simodo::lsp
{

enum class DiagnosticSeverity
{
    Error       = 1,
    Warning     = 2,
    Information = 3,
    Hint        = 4
};

enum class SymbolKind
{
	File        = 1,
	Module      = 2,
	Namespace   = 3,
	Package     = 4,
	Class       = 5,
	Method      = 6,
	Property    = 7,
	Field       = 8,
	Constructor = 9,
	Enum        = 10,
	Interface   = 11,
	Function    = 12,
	Variable    = 13,
	Constant    = 14,
	String      = 15,
	Number      = 16,
	Boolean     = 17,
	Array       = 18,
	Object      = 19,
	Key         = 20,
	Null        = 21,
	EnumMember  = 22,
	Struct      = 23,
	Event       = 24,
	Operator    = 25,
	TypeParameter=26,
};

std::u16string getDiagnosticSeverityString(DiagnosticSeverity severity);
std::u16string getSymbolKindString(SymbolKind kind);

}

#endif // simodo_lsp_LspData_H
