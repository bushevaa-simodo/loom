#ifndef _simodo_lsp_ServerCapabilities_h
#define _simodo_lsp_ServerCapabilities_h

#include <string>
#include <vector>

namespace simodo::lsp
{

inline const std::u16string POSITION_ENCODING_UTF16 = u"utf-16";

/**
 * Defines how the host (editor) should sync document changes to the language
 * server.
 */
enum class TextDocumentSyncKind
{
	/**
	 * Documents should not be synced at all.
	 */
    None = 0,

	/**
	 * Documents are synced by always sending the full content
	 * of the document.
	 */
    Full = 1,

	/**
	 * Documents are synced by sending the full content on open.
	 * After that only incremental updates to the document are
	 * sent.
	 */
    Incremental = 2,
};

struct TextDocumentSyncOptions
{
	/**
	 * Open and close notifications are sent to the server. If omitted open
	 * close notifications should not be sent.
	 */
	bool                    openClose	 = false;

	/**
	 * Change notifications are sent to the server. See
	 * TextDocumentSyncKind.None, TextDocumentSyncKind.Full and
	 * TextDocumentSyncKind.Incremental. If omitted it defaults to
	 * TextDocumentSyncKind.None.
	 */
	TextDocumentSyncKind    change = TextDocumentSyncKind::None;
};

struct SemanticTokensLegend
{
	/**
	 * The token types a server uses.
	 */
    std::vector<std::u16string> tokenTypes;

	/**
	 * The token modifiers a server uses.
	 */
	std::vector<std::u16string> tokenModifiers;
};

struct SemanticTokensOptions
{
	/**
	 * The legend used by the server
	 */
	SemanticTokensLegend    legend;

    //...
};

/**
 * Completion item.
 *
 * @since 3.17.0
 */
struct CompletionItem
{
	/**
	 * The server has support for completion item label
	 * details (see also `CompletionItemLabelDetails`) when receiving
	 * a completion item in a resolve call.
	 *
	 * @since 3.17.0
	 */
	bool labelDetailsSupport = false;
};

/**
 * Completion options.
 */
struct CompletionOptions
{
	/**
	 * The additional characters, beyond the defaults provided by the client (typically
	 * [a-zA-Z]), that should automatically trigger a completion request. For example
	 * `.` in JavaScript represents the beginning of an object property or method and is
	 * thus a good candidate for triggering a completion request.
	 *
	 * Most tools trigger a completion request automatically without explicitly
	 * requesting it using a keyboard shortcut (e.g. Ctrl+Space). Typically they
	 * do so when the user starts to type an identifier. For example if the user
	 * types `c` in a JavaScript file code complete will automatically pop up
	 * present `console` besides others as a completion item. Characters that
	 * make up identifiers don't need to be listed here.
	 */
	std::vector<std::u16string> triggerCharacters;

	/**
	 * The list of all possible characters that commit a completion. This field
	 * can be used if clients don't support individual commit characters per
	 * completion item. See client capability
	 * `completion.completionItem.commitCharactersSupport`.
	 *
	 * If a server provides both `allCommitCharacters` and commit characters on
	 * an individual completion item the ones on the completion item win.
	 *
	 * @since 3.2.0
	 */
	// std::vector<std::u16string> allCommitCharacters;

	/**
	 * The server provides support to resolve additional
	 * information for a completion item.
	 */
	bool resolveProvider = false;

	/**
	 * The server supports the following `CompletionItem` specific
	 * capabilities.
	 *
	 * @since 3.17.0
	 */
	CompletionItem completionItem;
};

struct ServerCapabilities
{
	/**
	 * The position encoding the server picked from the encodings offered
	 * by the client via the client capability `general.positionEncodings`.
	 *
	 * If the client didn't provide any position encodings the only valid
	 * value that a server can return is 'utf-16'.
	 *
	 * If omitted it defaults to 'utf-16'.
	 *
	 * @since 3.17.0
	 */
    std::u16string          positionEncoding = POSITION_ENCODING_UTF16;

	/**
	 * Defines how text documents are synced. Is either a detailed structure
	 * defining each notification or for backwards compatibility the
	 * TextDocumentSyncKind number. If omitted it defaults to
	 * `TextDocumentSyncKind.None`.
	 */
    TextDocumentSyncOptions textDocumentSync;

	/**
	 * The server provides completion support.
	 */
	CompletionOptions		completionProvider;

	/**
	 * The server provides hover support.
	 */
	bool                    hoverProvider	 	= false;

	/**
	 * The server provides go to declaration support.
	 *
	 * @since 3.14.0
	 */
	bool                    declarationProvider	= false;

	/**
	 * The server provides goto definition support.
	 */
	bool                    definitionProvider	= false;

	/**
	 * The server provides find references support.
	 */
	bool                    referencesProvider	= false;

	/**
	 * The server provides document symbol support.
	 */
	bool                    documentSymbolProvider = false;

	/**
	 * The server provides semantic tokens support.
	 *
	 * @since 3.16.0
	 */
	SemanticTokensOptions   semanticTokensProvider; 
};

}

#endif //_simodo_lsp_ServerCapabilities_h
