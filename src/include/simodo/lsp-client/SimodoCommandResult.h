#ifndef _simodo_lsp_SimodoCommandResult_h
#define _simodo_lsp_SimodoCommandResult_h

#include "simodo/variable/Variable.h"

#include <string>
#include <vector>

namespace simodo::lsp
{

	enum class SimodoCommandReportType
	{
		plainText = 0,
		markdownText = 1,
	};

	struct SimodoCommandReport
	{
		std::u16string 			id;
		std::u16string 			title;
		SimodoCommandReportType type = SimodoCommandReportType::plainText;
		std::u16string 			text;
	};

	struct SimodoCommandResult
	{
		std::u16string					 uri;
		std::vector<SimodoCommandReport> commandResult;
	};

	std::vector<SimodoCommandReport> getSimodoCommandResult(const simodo::variable::Value & result_value);
	bool parseSimodoCommandResult(const simodo::variable::Value & result_value, SimodoCommandResult & result);
}

#endif //_simodo_lsp_SimodoCommandResult_h
