/**
 * @file WorkFunction.hpp
 * @author Michael Fetisov (fetisov.michael@bmstu.ru)
 * @brief 
 * @version 0.1
 * @date 2023-08-18
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef simodo_tp_WorkFunction_H
#define simodo_tp_WorkFunction_H

#include "simodo/tp/WorkFunction.h"

#include <functional>

namespace simodo::tp
{

/**
 * @brief Задача для пула потока, позволяющая запускать на параллельное исполнение заданную функцию
 * 
 * @details
 * Используется в пуле потоков для передачи в пул потока функции. 
 * 
 * См. tp::ThreadPool::submit(std::function<void()> function).
 */
class WorkFunction : public Task_interface
{
    std::function<void()>   _function;

public:
    WorkFunction(std::function<void()> function) : _function(function) {}

    virtual void work() override
    {
        _function();
    }
};

}

#endif // simodo_tp_WorkFunction_H
