/**
 * @file Version.h
 * @author Фетисов Михаил Вячеславович (fetisov.michael@yandex.ru)
 * @brief Определения номеров текущей версии библиотеки.
 * @version 0.1
 * @date 2023-02-16
 * 
 * @copyright MIT: Michael Fetisov, Anton Bushev
 * 
 */
#ifndef simodo_LibVersion_H
#define simodo_LibVersion_H

namespace simodo
{

typedef unsigned short version_part_t;
typedef version_part_t version_major_t;
typedef version_part_t version_minor_t;

class version_t
{
    version_major_t _major;
    version_minor_t _minor;

public:
    version_t() = delete;
    version_t(version_major_t major, version_minor_t minor)
        : _major(major), _minor(minor)
    {}

    version_major_t major() const { return _major; }
    version_minor_t minor() const { return _minor; }
};

inline const version_major_t LibVersion_Major  = 0; ///< Старший номер версии библиотеки
inline const version_minor_t LibVersion_Minor  = 1; ///< Младший номер версии библиотеки

inline const version_t lib_version() { return {LibVersion_Major,LibVersion_Minor}; }

}

#endif // simodo_LibVersion_H