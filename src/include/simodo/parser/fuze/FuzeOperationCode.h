/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_parser_fuze_FuzeOperationCode
#define simodo_parser_fuze_FuzeOperationCode

/*! \file OperationCode.h
    \brief Мнемокоды операций языка Fuze
*/

#include <cstdint>
#include <string>

namespace simodo::parser
{
    inline const std::u16string FUZE_HOST_NAME {u"fuze"};

    /*! \brief Мнемокоды операций языка Fuze
     */
    enum class FuzeOperationCode : uint16_t
    {
        None            = 0,    ///< Пустая операция
        Main            = 1,    ///< Явная идентификация главного правила грамматики
        GlobalScript    = 2,    ///< Определение общего сценария
        Production      = 3,    ///< Группа операций описания продукции
            Production_Pattern    = 31,    ///< Перечень токенов образца
            Production_Direction  = 32,    ///< Направление разбора в случае неопределённости грамматики
            Production_Script     = 33,    ///< Семантическая вставка
        RemoveProduction= 4,    ///< Группа операций удаления продукции
        Reference       = 5,    ///< Информация о ссылке, по которой был выполнен оператор include
    };

    /*!
     * \brief Преобразовывает код операции в строку
     * \param op Код операции
     * \return   Строковое представление кода операции
     */
    const char * getOperationCodeName(FuzeOperationCode op) noexcept;

}

#endif // simodo_parser_fuze_FuzeOperationCode
