/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_parser_fuze_fuze_file_extension
#define simodo_parser_fuze_fuze_file_extension

/*! \file fuze_file_extension.h
    \brief Класс разбора синтаксических правил на подмножестве языка SIMODO fuze методом рекурсивного спуска
*/

#include <string>

namespace simodo::parser
{
    inline const std::string FUZE_FILE_EXTENSION {".fuze"};  ///< Расширение файлов метаязыка (для раскрутки)

}

#endif // simodo_parser_fuze_fuze_file_extension
