/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_parser_fuze_BaseOperationCode
#define simodo_parser_fuze_BaseOperationCode

/*! \file BaseOperationCode.h
    \brief Мнемокоды операций языка SBL
*/

#include <cstdint>
#include <string>

namespace simodo::parser
{
    inline const std::u16string SCRIPT_HOST_NAME {};

    /*! \brief Мнемокоды операций языка SBL

        Начальные операции обеспечивают работу подмножества SBL, которое необходимо для раскрутки 
        (см. class FuzeSblRdp).

     */
    enum class BaseOperationCode : uint16_t
    {
        // Начальные операции для выполнения раскрутки.
        // Используются в семантических вставках при разборе правил грамматики (кроме Print).

        None            = 0,    ///< Пустая операция
        PushConstant    = 1,    ///< Операция помещения на стек константы
        PushVariable    = 2,    ///< Операция помещения на стек значения переменной
        ObjectElement   = 3,    ///< Операция взятия адреса члена кортежа
        FunctionCall    = 4,    ///< Операция вызова функции
        ProcedureCheck  = 5,    ///< Проверка вызова процедуры 
        Print           = 6,    ///< Оператор вывода сообщения на консоль
        Block           = 7,    ///< Начало группы операторов
        Pop             = 8,    ///< Снятие переменной со стека
        LastOperation,
    };

    /*!
     * \brief Преобразовывает мнемокод операции в строку
     * \param op Мнемокод операции
     * \return   Строковое представление мнемокода операции
     */
    const char * getBaseOperationCodeName(BaseOperationCode op) noexcept;

}

#endif // simodo_parser_fuze_BaseOperationCode
