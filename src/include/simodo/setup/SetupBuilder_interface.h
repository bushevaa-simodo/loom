/**
 * @file SetupBuilder_interface.h
 * @author Michael Fetisov (fetisov.michael@bmstu.ru)
 * @brief 
 * @version 0.1
 * @date 2022-08-26
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

#include <vector>
#include <string>

namespace simodo::setup
{
    class SetupDescription;

    class SetupBuilder_interface
    {
    public:
        virtual ~SetupBuilder_interface() = default;

        virtual SetupDescription build(const std::string & id) = 0;
        virtual std::string errors() const = 0;
    };
}
