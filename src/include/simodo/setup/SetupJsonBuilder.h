/**
 * @file SetupJsonBuilder.h
 * @author Michael Fetisov (fetisov.michael@bmstu.ru)
 * @brief 
 * @version 0.1
 * @date 2022-08-26
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

#include "simodo/setup/SetupBuilder_interface.h"

namespace simodo::setup
{
    class SetupJsonBuilder : public SetupBuilder_interface
    {
        std::string _errors;

    public:
        virtual SetupDescription build(const std::string & setup_file) override;
        virtual std::string errors() const override { return _errors; }
    };
}
