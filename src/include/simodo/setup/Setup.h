/**
 * @file Setup.h
 * @author Michael Fetisov (fetisov.michael@bmstu.ru)
 * @brief 
 * @version 0.1
 * @date 2022-08-26
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

#include "simodo/setup/SetupStructure.h"
#include "simodo/setup/SetupBuilder_interface.h"

#include <string>
#include <vector>
#include <memory>

namespace simodo::setup
{
    class Setup
    {
        std::string                     _default_setup_file;
        std::string                     _work_setup_file;

        SetupDescription                _description;
        std::unique_ptr<SetupBuilder_interface> 
                                        _builder;

    public:
        Setup() = delete;
        Setup(std::string default_setup_file, std::string work_setup_file);

        const std::string &         default_setup_file() const { return _default_setup_file; }
        const std::string &         work_setup_file()    const { return _work_setup_file; }
        const SetupDescription &    setup()              const { return _description; }
        // const SetupBuilder_interface & builder()         const { return *_builder.get(); }
        std::string                 error()              const { return _builder->errors(); }

    public:
        bool load();
        bool loadDefault();
        bool save();

    public:
        SetupStructure              structure(const std::string & id) const;
        variable::Value             value(const std::string & id) const;
        void                        setValue(const std::string & id, const variable::Value & value);
    };
}

