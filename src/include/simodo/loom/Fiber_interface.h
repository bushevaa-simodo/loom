/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_loom_Fiber_interface
#define simodo_loom_Fiber_interface

/** 
 * @file Fiber_interface.h
 * @brief Определения для Fiber_interface
 */

#include "simodo/loom/FiberStatus.h"

#include <memory>

namespace simodo::loom 
{
    /**
     * @brief Волокно всегда знает, как оно может вплетаться, а также текущее своё состояние.
     * 
     * А вот, что мне вчера приснилось:
     * 
     * Некоторые волокна могут перегружаться на разные челноки и возвращаться обратно.
     * Это позволяет плести не только верёвки, но и прекрасную многомерную ткань мира!
     * 
     * Добивайтесь поставки только перегружаемых волокон!
     * 
     * Волокно, перегруженное на инопланетный челнок и ушедшее за край мира, всё равно продолжает 
     * участвовать в плетении ткани здесь, что формирует красивые и загадочные аномалии.
     * 
     * Но для этого нам нужны возможности распространяться и управлять этим...
     * 
     * @note Доктор Борменталь: "Хм... Любопытная шизофазия. Не находите, профессор?"
     * 
     * @note Профессор Преображенский: "Не уверен... В этих фразах всё же есть толика смысла."
     * 
     */
    class Fiber_interface
    {
    public:
        virtual ~Fiber_interface() = default;

        virtual FiberStatus             start()         = 0;
        virtual FiberStatus             tieKnot()       = 0;
        virtual void                    finish()        = 0;
        virtual bool                    isReady() const = 0;

        virtual const Fiber_interface * getWaitingFor() = 0;
    };

}

#endif // simodo_loom_Hairstyle_interface
