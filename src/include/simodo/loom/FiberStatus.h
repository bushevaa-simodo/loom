/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_loom_FiberStatus
#define simodo_loom_FiberStatus

/** 
 * @file FiberStatus.h
 * @brief Определения для FiberStatus
 */

#include <string>

namespace simodo::loom 
{
    /**
     * @brief Внимательно следи за состоянием волокна! 
     * 
     */
    enum class FiberStatus
    {
        Queued,     ///< Волосок находится в очереди
        Flow,       ///< Волосок вплетается
        Complete,   ///< Волосок закончилась
        Paused,      ///< Волосок остановлена, т.к. весь станок приостановлен, челнок не освобождён
        Delayed,    ///< Волосок отложена, т.к. либо ждёт другую нить, либо запуска трафарета, челнок освобождён и может работать с другой нитью
    };

    const char * getFiberStatusName(FiberStatus status);
}

#endif // simodo_loom_FiberStatus
