/**
 * @file Service.h
 * @author Фетисов Михаил Вячеславович (fetisov.michael@yandex.ru)
 * @brief Определения сервисов оболочки.
 * @version 0.1
 * @date 2023-02-16
 * 
 * @copyright MIT: Michael Fetisov, Anton Bushev
 * 
 */
#ifndef simodo_shell_Service_H
#define simodo_shell_Service_H

#include <cstdint>

namespace simodo::shell
{

typedef uint32_t service_t;

inline const service_t PS_None              = 0x00000000;
inline const service_t PS_Doc_New           = 0x00000001;   ///< Можно создавать новый документ
inline const service_t PS_Doc_Open          = 0x00000002;   ///< Можно открыть документ через диалог Open
inline const service_t PS_Doc_Save          = 0x00000004;   ///< Документ может быть сохранён (и сохранён как...)
inline const service_t PS_Doc_Print         = 0x00000008;   ///< Документ может быть выведен на принтер
inline const service_t PS_Doc_ExportToPdf   = 0x00000010;   ///< Документ может быть экспортирован в PDF
inline const service_t PS_Doc_Export        = 0x00000020;   ///< Документ имеет возможность для экспорта в другой формат
inline const service_t PS_Doc_UndoRedo      = 0x00000040;   ///< Документ умеет откатывать изменения
inline const service_t PS_Doc_Copy          = 0x00000080;   ///< Можно делать копию документа или его фрагмента в буфер обмена
inline const service_t PS_Doc_Paste         = 0x00000100;   ///< Можно вставлять в документ из буфера обмена
inline const service_t PS_Doc_Zoom          = 0x00000200;   ///< Можно изменять масштаб представления документа
inline const service_t PS_Doc_Find          = 0x00000400;   ///< Можно использовать стандартный поиск в документе
inline const service_t PS_Doc_Replace       = 0x00000800;   ///< Можно использовать стандартную замену в документе
inline const service_t PS_Doc_Run           = 0x00001000;   ///< Документ поддерживает механизм запуска модели

inline const service_t PS_Doc_ReadOnly      = 0x00100000;   ///< Документ только для чтения
inline const service_t PS_Doc_Loadable      = 0x00200000;   ///< Документ загружаемый
inline const service_t PS_Doc_Splittable    = 0x00400000;   ///< Документ можно разделить на несколько представлений

inline const service_t PS_Pan_ShowOnStart   = 0x00000001;   ///< Панель будет отображаться при старте редактора
inline const service_t PS_Pan_Singleton     = 0x00000002;   ///< Панель может быть только в единственном экземпляре, 
                                                            ///< оболочка создаст её при старте
inline const service_t PS_Pan_Find          = 0x00000004;   ///< Панель может использоваться для поиска текста в документе
inline const service_t PS_Pan_Replace       = 0x00000008;   ///< Панель может использоваться для замены текста в документе

inline const service_t PS_Run_AutoPreview   = 0x00000001;   ///< Модуль моделирования поддерживает автоматический запуск
// inline const service_t PS_Run_AutoPreviewDef= 0x00000002;   ///< Модуль моделирования поддерживает автоматический запуск по умолчанию

/**
 * @brief Инлайн-функция проверки флага сервиса.
 * 
 * @param parameters перечень поддерживаемых сервисов,
 * @param service флаг проверяемого сервиса или проверяемой обязательной группы сервисов
 * @return true - сервис или вся группа поддерживается, 
 * @return false - не поддерживается
 */
inline static bool check(service_t parameters, service_t service)
{
    return (parameters & service) == service;
}

}

#endif // simodo_shell_Service_H
