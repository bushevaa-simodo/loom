/**
 * @file Plugin_interface.h
 * @author Фетисов Михаил Вячеславович (fetisov.michael@yandex.ru)
 * @brief Объявление базового интерфейса плагинов оболочки.
 * @version 0.1
 * @date 2023-02-18
 * 
 * @copyright MIT: Michael Fetisov, Anton Bushev
 * 
 */
#ifndef simodo_shell_Plugin_interface_H
#define simodo_shell_Plugin_interface_H

#include "simodo/shell/Version.h"
#include "simodo/shell/Service.h"

#include <QString>

#include <utility>

namespace simodo::shell
{

/**
 * @brief Интерфейс плагина оболочки.
 * 
 * @details
 * Интерфейс помогает унифицировать общие требования ко всем плагинам оболочки и позволяет
 * загружать их единообразно.
 * 
 */
class Plugin_interface
{
public:
    virtual ~Plugin_interface() = default;

    /**
     * @brief Главный и минорный номера версии плагина.
     * 
     * @details 
     * Главный номер должен совпадать с главной версией оболочки, 
     * а минорный не должен превышать минорную версию оболочки.
     * 
     * @return главный и минорный номера версии плагина
     */
    virtual version_t version() const = 0;

    /**
     * @brief Сообщает какие сервисы оболочки поддерживает плагин. 
     * 
     * @return битовая маска поддерживаемых сервисов
     */
    virtual service_t supports() const = 0;

    /**
     * @brief Идентификатор (мнемокод) плагина.
     * 
     * Идентификатор представления может использоваться, например, чтобы подсказать оболочке
     * дополнительное представление для данного типа документа (см. alternate_view_mnemonics).
     * 
     * @attention Идентификатор (мнемокод) нельзя переводить, т.к. он должен быть постоянным 
     * и уникальным для того, чтобы можно было ссылаться на данный плагин из других плагинов.
     * 
     * @return идентификатор (мнемокод) плагина 
     */
    virtual QString id() const = 0;
};

}

#endif // simodo_shell_Plugin_interface_H
