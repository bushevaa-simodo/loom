/**
 * @file PanelAdaptorModeling_interface.h
 * @author Фетисов Михаил Вячеславович (fetisov.michael@yandex.ru)
 * @brief Объявление интерфейса для обработки результатов моделирования и управления моделированием
 * у адаптеров (как документов, так и панелей).
 * @todo Для документов пока не используется 
 * @version 0.1
 * @date 2023-04-04
 *
 * @copyright MIT: Michael Fetisov, Anton Bushev
 *
 */
#ifndef simodo_shell_AdaptorModeling_interface_H
#define simodo_shell_AdaptorModeling_interface_H

#include <QString>
#include <QVector>

namespace simodo::shell
{

/**
 * @brief Интерфейс для обработки результатов моделирования, как во время, так и после моделирования.
 * 
 * @details
 * Какие именно результаты моделирования обрабатываются инкапсулировано в ранере моделирования (плагин Panel_plugin).
 * 
 * Для того, чтобы иметь возможность работать с данными моделирования адаптер панели или документа должны предоставить 
 * доступ к этому интерфейсу, т.е. реализовать метод getModelingInterface.
 *
 */
class AdaptorModeling_interface
{
public:
    virtual ~AdaptorModeling_interface() = default;

    /**
     * @brief Нотификация о запуске моделирования
     * 
     */
    virtual void started() = 0;

    /**
     * @brief Нотификация о приостановке моделирования
     * 
     */
    virtual void paused() = 0;

    /**
     * @brief Нотификация о завершении моделирования
     * 
     */
    virtual void stoped() = 0;

    /**
     * @brief Получение данных
     *
     * Этот метод вызывается, если нужно отобразить или обработать изменения в соответствии с полученными данными.
     *
     * @param func наименование операции (функции)
     * @param data данные в виде массива строк
     * @return true - удалось интерпретировать и отобразить данные,
     * @return false - данные не удалось интерпретировать
     */
    virtual bool acceptData(const QString & func, QVector<QString> & data) = 0;
};

}

#endif // simodo_shell_AdaptorModeling_interface_H