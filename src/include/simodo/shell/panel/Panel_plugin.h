/**
 * @file Panel_plugin.h
 * @author Фетисов Михаил Вячеславович (fetisov.michael@yandex.ru)
 * @brief Объявления интерфейса плагина панели.
 * @version 0.1
 * @date 2023-02-16
 *
 * @copyright MIT: Michael Fetisov, Anton Bushev
 *
 */
#ifndef simodo_shell_panel_Panel_plugin_H
#define simodo_shell_panel_Panel_plugin_H

#include "simodo/shell/Plugin_interface.h"
#include "simodo/shell/Service.h"
#include "simodo/shell/access/Access_interface.h"
#include "simodo/shell/panel/PanelAdaptor_interface.h"

namespace simodo::shell
{

/**
 * @brief Интерфейс плагина панели.
 *
 * Плагин панели является фабрикой для адаптера панели, который является посредником между
 * оболочкой и виджетом панели.
 *
 * @details
 * Для управления панелями используются стандартные средства Qt. Виджет панели оболочка
 * размещает внутри создаваемой ею панели (на базе QDockWidget).
 *
 */
class Panel_plugin : public Plugin_interface
{
public:
    /**
     * @brief Создание нового адаптера панели.
     *
     * @param shell_access ссылка на интерфейс функций оболочки
     * @return указатель на созданный адаптер панели
     */
    virtual PanelAdaptor_interface * createPanelAdaptor(Access_interface & shell_access) = 0;

    /**
     * @brief Обозначение для всех панелей данного плагина
     * 
     * @details Это обозначение будет выводиться в заголовок всех панелей данного плагина. После него будет ставиться 
     * двоеточие и через пробел заголовок конкретной панели, например: "Chart: БЛА"
    */
    virtual QString designation() const = 0;

    /**
     * @brief Определение разрешённых положений панели.
     *
     * @return битовая маска возможных положений панели
     */
    virtual Qt::DockWidgetAreas allowed_areas() const = 0;

    /**
     * @brief Определение начального положения панели по умолчанию.
     *
     * @return битовая маска начальных положений панели
     */
    virtual Qt::DockWidgetArea attach_to() const = 0;

    /**
     * @brief Определяет перечень типов документов с которыми работает панель.
     *
     * @details
     * Возвращает список мнемокодов документов, для которых панель будет активна.
     * Если возвращается пустой набор, значит панель работает со всеми типами документов.
     *
     * @return список мнемокодов документов
     */
    virtual QSet<QString> workWithDocuments() const = 0;
};

}

#define PanelPlugin_iid "BMSTU.SIMODO.shell.plugin.Panel"

Q_DECLARE_INTERFACE(simodo::shell::Panel_plugin, PanelPlugin_iid)

#endif // simodo_shell_panel_Panel_plugin_H
