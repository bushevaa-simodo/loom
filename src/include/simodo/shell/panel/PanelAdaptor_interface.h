/**
 * @file PanelAdaptor_interface.h
 * @author Фетисов Михаил Вячеславович (fetisov.michael@yandex.ru)
 * @brief Объявления интерфейса адаптера панели.
 * @version 0.1
 * @date 2023-02-16
 *
 * @copyright MIT: Michael Fetisov, Anton Bushev
 *
 */
#ifndef simodo_shell_panel_PanelAdaptor_interface_H
#define simodo_shell_panel_PanelAdaptor_interface_H

#include "simodo/shell/Service.h"

#include <QWidget>

class QJsonObject;

namespace simodo::shell
{
    class AdaptorModeling_interface;

/**
 * @brief Интерфейс адаптера панели.
 *
 * Адаптер панели является посредником между оболочкой и виджетом панели.
 *
 */
class PanelAdaptor_interface
{
public:
    virtual ~PanelAdaptor_interface() = default;

    /**
     * @brief Виджет панели.
     *
     * @return виджет панели
     */
    virtual QWidget * panel_widget() = 0;

    /**
     * @brief Заголовок панели.
     *
     * @details
     * Заголовок панели может быть изменён с помощью метода Access_interface::sendDataToPanel.
     * 
     * Также заголовок участвует в идентификации конкретной панели для передачи для неё информации, 
     * а также для сохранения её места и геометрии для последующего восстановления. 
     *
     * @return заголовок панели
     */
    virtual QString title() const = 0;

    /**
     * @brief Выполнить инициализации панели.
     *
     * Оболочка вызывает этот метод после каких-либо глобальных изменений. Например,
     * после смены текущего (домашнего) каталога.
     *
     */
    virtual void reset() = 0;

    /**
     * @brief Сообщение от оболочки, что изменён текущий документ (не содержимое, а когда выбран 
     * другой активный документ или закрыт последний).
     *
     * Если последний документ закрыт (оболочка не имеет открытых документов), в качестве
     * пути к файлу документа передаётся пустая строка.
     *
     * @param path полный путь к файлу документа
     */
    virtual void changedCurrentDocument(const QString & path) = 0;

    /**
     * @brief Активирован сервис.
     *
     * Полезно, если панель поддерживает несколько сервисов сразу. Оболочка сообщает, какой
     * конкретно активирован.
     *
     * @attention Данный метод вызывается, не для выполнения конкретного сервиса, а для переключения
     * на отображение какой-либо информации для его возможного будущего выполнения. В качестве примера
     * можно посмотреть работу панели поиска и замены.
     *
     * @param function активированный сервис
     * @return true - переключение удалось,
     * @return false - переключение не удалось
     */
    virtual bool activateService(shell::service_t function) = 0;

    /**
     * @brief Получение данных для панели.
     *
     * Этот метод вызывается, если на панели нужно отобразить изменения в соответствии с полученными данными.
     *
     * @param data данные в структуре JSON
     * @return true - удалось интерпретировать и отобразить данные,
     * @return false - данные не удалось интерпретировать
     */
    virtual bool acceptData(const QJsonObject & data) = 0;

    /**
     * @brief Предоставление указателя на интерфейс моделирования
     * 
     * @return Указатель на интерфейс моделирования или nullptr, если интерфейс моделирования не 
     * поддерживается
     */
    virtual AdaptorModeling_interface * getModelingInterface() = 0;
};

}

#endif // simodo_shell_panel_PanelAdaptor_interface_H
