/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_module_Module_interface
#define simodo_module_Module_interface

/*! \file Module_interface.h
    \brief Интерфейс управляющего кода модуля
*/

#include "simodo/variable/Variable.h"
#include "simodo/LibVersion.h"

#include <memory>

namespace simodo::variable
{
    class Module_interface
    {
    public:
        virtual ~Module_interface() = default;

        /**
         * @brief Главный и минорный номера версии модуля.
         * 
         * @details 
         * Главный номер должен совпадать с главной версией библиотеки из simodo/version.h, 
         * а минорный не должен превышать минорную версию библиотеки.
         * 
         * @return главный и минорный номера версии модуля
         */
        virtual version_t version() const = 0;

        virtual Object instantiate(std::shared_ptr<Module_interface> module_object) = 0;
    };

}

#endif // simodo_module_Module_interface
