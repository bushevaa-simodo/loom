#ifndef simodo_variable_json_Rpc_h
#define simodo_variable_json_Rpc_h

#include "simodo/variable/Variable.h"

namespace simodo::variable
{

class JsonRpc
{
    std::string     _origin_json;
    variable::Value _value;
    bool            _is_valid  = false;
    std::u16string  _jsonrpc_version;
    std::u16string  _method;
    int64_t         _id = -1;

    variable::Value _fail;
    const variable::Value * _params = nullptr;
    const variable::Value * _result = nullptr;
    const variable::Value * _error = nullptr;

public:
    JsonRpc() = default;
    explicit JsonRpc(const std::string & json);
    explicit JsonRpc(const std::u16string & json);
    explicit JsonRpc(variable::Value value);
    explicit JsonRpc(std::u16string method, variable::Value params, int64_t id);
    explicit JsonRpc(std::u16string method, int64_t id);
    explicit JsonRpc(std::u16string method, variable::Value params);
    // JsonRpc(std::u16string method); // Конфликтует с первым конструктором
    explicit JsonRpc(variable::Value result, int64_t id);
    explicit JsonRpc(int64_t code, std::u16string message, variable::Value data, int64_t id);
    explicit JsonRpc(int64_t code, std::u16string message, int64_t id);

    const variable::Value   value()     const { return _value; }
    bool                    is_valid()  const { return _is_valid; }
    const std::u16string &  jsonrpc_version() const { return _jsonrpc_version; }
    const std::u16string &  method()    const { return _method; }
    const variable::Value & params()    const;
    const variable::Value & result()    const;
    const variable::Value & error()     const;
    int64_t                 id()        const { return _id; }
    const std::string &     origin()    const { return _origin_json; }

private:
    void setupMembers();
};

}

#endif // simodo_variable_json_Rpc_h