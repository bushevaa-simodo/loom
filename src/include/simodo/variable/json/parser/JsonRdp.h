/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_variable_json_parser_Parser
#define simodo_variable_json_parser_Parser

/*! \file Parser.h
    \brief Загрузка (разбор) JSON из файла или потока во внутренний формат класса variable::Value
*/

#include "simodo/variable/json/parser/JsonAnalyzeDataBuilder_interface.h"
#include "simodo/variable/Variable.h"

#include "simodo/inout/reporter/Reporter_abstract.h"
#include "simodo/inout/token/Parser_interface.h"
#include "simodo/inout/token/InputStream_interface.h"
#include "simodo/inout/token/Tokenizer.h"
#include "simodo/inout/token/RdpBaseSugar.h"

#include <string>
#include <memory>

namespace simodo::variable
{
    class JsonRdp : public inout::Parser_interface, public inout::RdpBaseSugar
    {
        inout::uri_set_t                    _files;
        JsonAnalyzeDataBuilder_interface &  _builder;
        const std::string                   _json_file;
        Value &                             _value;
        mutable std::unique_ptr<inout::Tokenizer> _tokenizer;
        mutable bool                        _ok = true;

    public:
        JsonRdp(inout::Reporter_abstract & m, const std::string json_file, Value & value); 
        JsonRdp(inout::Reporter_abstract & m, JsonAnalyzeDataBuilder_interface & builder, const std::string json_file, Value & value); 

        virtual bool    parse() override;
        virtual bool    parse(inout::InputStream_interface & stream) override;

    protected:
        void    parseValue(const inout::Token & value_token, Value & value) const;
        void    parseObject(const inout::Token & open_brace_token, Value & value) const;
        void    parseArray(const inout::Token & open_brace_token, Value & value) const;
    };
}

#endif // simodo_variable_json_parser_Parser
