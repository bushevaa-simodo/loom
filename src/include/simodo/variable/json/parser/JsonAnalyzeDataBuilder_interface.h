/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_variable_json_parser_JsonAnalyzeDataBuilder_interface
#define simodo_variable_json_parser_JsonAnalyzeDataBuilder_interface

/*! \file JsonAnalyzeDataBuilder_interface.h
    \brief Загрузка (разбор) JSON из файла или потока во внутренний формат класса variable::Value
*/

#include "simodo/inout/token/Token.h"

namespace simodo::variable
{
    class JsonAnalyzeDataBuilder_interface
    {
    public:
        virtual ~JsonAnalyzeDataBuilder_interface() = default;

        virtual void addGroup(const inout::Token & open_brace_token, 
                              const inout::Token & close_brace_token) = 0;
        virtual void addVariable(const inout::Token & variable_token) = 0;
        virtual void addConst(const inout::Token & const_token) = 0;
    };

}

#endif // simodo_variable_json_parser_JsonAnalyzeDataBuilder_interface
