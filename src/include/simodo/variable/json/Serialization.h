/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_variable_json_JsonSerialization
#define simodo_variable_json_JsonSerialization

/*! \file JsonSerialization.h
    \brief Загрузка и сохранение данных в формате JSON
*/

#include "simodo/variable/Variable.h"

namespace simodo::variable
{
    class JsonRpc;

    std::string loadJson(const std::string & file_name, Value & value);
    std::string loadJson(std::istream & in, Value & value, const std::string & file_name = "");

    bool saveJson(const Value & value, std::ostream & out, bool compress=true, bool skip_empty_parameters=false);
    bool saveJson(const Value & value, const std::string & file_name, bool compress=true, bool skip_empty_parameters=false);

    bool saveJson(const JsonRpc & rpc, std::ostream & out, bool compress=true, bool skip_empty_parameters=false);

    Value fromJson(const std::string & json_string);
    Value fromJson(const std::u16string & json_string);
    std::string  toJson(const Value & value, bool compress=true, bool skip_empty_parameters=false);
}

#endif // simodo_variable_json_JsonSerialization
