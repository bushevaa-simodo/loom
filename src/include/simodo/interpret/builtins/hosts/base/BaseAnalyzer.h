/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_interpret_host_base_HostAnalyzer
#define simodo_interpret_host_base_HostAnalyzer

/*! \file BaseAnalyzer.h
    \brief Анализ операторов SBL с использованием внутренней машиной SIMODO
*/

#include "simodo/interpret/builtins/hosts/base/BaseInterpret_abstract.h"
#include "simodo/interpret/SemanticDataCollector_interface.h"

#include <memory>

namespace simodo::interpret::builtins
{
    inline const size_t MAX_NUMBER_OF_MISTAKES = 5;

    class BaseAnalyzer: public BaseInterpret_abstract
    {
        SemanticDataCollector_interface & _collector;
        size_t                            _number_of_mistakes = 0;

        std::vector<simodo::inout::TokenLocation> _open_scope;

    public:
        BaseAnalyzer(Interpret * inter);
        BaseAnalyzer(Interpret * inter, SemanticDataCollector_interface & collector);

    public:
        virtual void importNamespace(std::u16string name, variable::Object ns, inout::TokenLocation location) override;

    public:
        virtual bool            checkInterpretType      (InterpretType interpret_type) const override { return interpret_type == InterpretType::Analyzer; }
        virtual InterpretState  performOperation        (const ast::Node & op) override;

    public:
        virtual InterpretState  before_start            () override;
        virtual InterpretState  before_finish           (InterpretState state) override;

        /// \note Перехватывать исключение AnalyzeException в следующих методах не нужно, 
        /// т.к. оно обрабатывается в BaseAnalyzer::performOperation
        virtual InterpretState  executePushValue        (const inout::Token & variable_name) override;
        virtual InterpretState  executeObjectElement    (const inout::Token & dot, const inout::Token & variable_name) override;
        virtual InterpretState  executeFunctionCall     (const ast::Node & op) override;
        virtual InterpretState  executeBlock            (const ast::Node & op) override;
    };
}

#endif // simodo_interpret_host_base_HostAnalyzer
