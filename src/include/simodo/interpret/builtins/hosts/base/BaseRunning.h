/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_interpret_host_base_HostPreview
#define simodo_interpret_host_base_HostPreview

/*! \file BaseRunning.h
    \brief Интерпретация операторов SBL внутренней машиной SIMODO
*/

#include "simodo/interpret/builtins/hosts/base/BaseInterpret_abstract.h"

namespace simodo::interpret::builtins
{
    class BaseRunning: public BaseInterpret_abstract
    {
    public:
        BaseRunning(Interpret * inter);

    public:
        virtual bool    checkInterpretType(InterpretType interpret_type) const override { return interpret_type == InterpretType::Preview; }
    };

}

#endif // simodo_interpret_host_base_HostPreview
