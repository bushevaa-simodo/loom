/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_interpret_host_fuze_HostPreview
#define simodo_interpret_host_fuze_HostPreview

/*! \file FuzeRunning.h
    \brief Интерпретация операторов Fuze внутренней машиной SIMODO
*/

#include "simodo/interpret/builtins/hosts/fuze/FuzeInterpret_abstract.h"

namespace simodo::interpret::builtins
{
    class FuzeRunning: public FuzeInterpret_abstract
    {
    public:
        FuzeRunning(Interpret * inter, 
                std::string grammar_name,
                parser::Grammar & grammar,
                parser::TableBuildMethod method=parser::TableBuildMethod::none,
                bool need_strict_rule_consistency=true);

    // Наследие SemanticModule_interface

    public:
        virtual bool checkInterpretType(InterpretType interpret_type) const override { return interpret_type == InterpretType::Preview; }
    };

}

#endif // simodo_interpret_host_fuze_HostPreview
