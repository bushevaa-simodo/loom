/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_fuze_LexicalParametersModule
#define simodo_fuze_LexicalParametersModule

/*! \file LexicalParametersModule.h
    \brief Модуль для загрузки параметров лексики (только для внутреннего использования)
*/

#include "simodo/variable/Module_interface.h"
#include "simodo/inout/token/LexicalParameters.h"

namespace simodo::interpret::builtins
{
    class LexicalParametersModule : public variable::Module_interface
    {
        inout::LexicalParameters & _lex;

    public:
        LexicalParametersModule() = delete;

        LexicalParametersModule(inout::LexicalParameters & lex);
        virtual ~LexicalParametersModule();

        virtual version_t version() const override { return lib_version(); }

        virtual variable::Object instantiate(std::shared_ptr<variable::Module_interface> module_object) override;

        // virtual variable::ModuleFactory_interface * factory() override;

        inout::LexicalParameters & operator () () { return _lex; }
    };

}

#endif // simodo_fuze_LexicalParametersModule
