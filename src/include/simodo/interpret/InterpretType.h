/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_interpret_InterpretType
#define simodo_interpret_InterpretType

/*! \file InterpretType.h
    \brief Тип владельца семантики.
*/

namespace simodo::interpret
{
    /*!
     * \brief Тип владельца семантики
     */
    enum class InterpretType
    {
        Preview     = 0,
        Analyzer    = 1,
        Translate   = 2,
    };

}

#endif // simodo_interpret_InterpretType
