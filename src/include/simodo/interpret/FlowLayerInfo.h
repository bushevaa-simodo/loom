/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_interpret_FlowLayerInfo
#define simodo_interpret_FlowLayerInfo

/*! \file Interpret_interface.h
    \brief .
*/

#include "simodo/ast/Node.h"
#include "simodo/interpret/StackOfNames_interface.h"

#include <functional>

namespace simodo::interpret
{
    /**
     * @brief Информация о проходе (состояние прохода) по уровню вложенности
    */
    struct FlowLayerInfo
    {
        const ast::Node &                       code;
        std::function<void (const FlowLayerInfo &)>   on_layer_end;
        boundary_index_t                        boundary_index;
        bool                                    error_sign = false;
        size_t                                  index = 0;
        size_t                                  internal_work_index = 0;
    };

}

#endif // simodo_interpret_FlowLayerInfo
