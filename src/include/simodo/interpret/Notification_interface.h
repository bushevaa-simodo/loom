/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_interpret_Notification_interface
#define simodo_interpret_Notification_interface

/*! \file Notification_interface.h
    \brief Интерфейс нотификаций для использования при интерпретации
*/

#include "simodo/variable/Variable.h"

namespace simodo::interpret
{
    /*!
     * \brief The Notification_interface class
     */
    class Notification_interface
    {
    public:
        virtual ~Notification_interface() = default;

        virtual void notifyDeclared(const variable::Variable & ) = 0;
        virtual void notifyInitiated(const variable::Variable & ) = 0;
        virtual void notifyNameUsed(const variable::Variable & , const inout::TokenLocation & ) = 0;     
        virtual void notifyRef(const inout::Token & , const std::u16string & ) = 0;
        virtual void notifyBeforeFunctionCalling(const variable::Variable & ) = 0;
        virtual void notifyRemoteFunctionLaunch(const inout::TokenLocation & ) = 0;
    };

}

#endif // simodo_interpret_Notification_interface
