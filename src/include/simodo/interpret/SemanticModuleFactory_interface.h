/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_interpret_InterpretFactory_interface
#define simodo_interpret_InterpretFactory_interface

/*! \file SemanticModuleFactory_interface.h
    \brief Интерфейс держателя семантических правил
*/

#include "simodo/interpret/SemanticModule_interface.h"
#include "simodo/LibVersion.h"

#include <memory>

namespace simodo::interpret
{
    /*!
     * \brief The SemanticModuleFactory_interface class
     */
    class SemanticModuleFactory_interface
    {
    public:
        virtual ~SemanticModuleFactory_interface() = default;

        virtual version_t version() const = 0;
        virtual SemanticModule_interface * create(Interpret_interface & inter) const = 0;
    };

}

#endif // simodo_interpret_InterpretFactory_interface
