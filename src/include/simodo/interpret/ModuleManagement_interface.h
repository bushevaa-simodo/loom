/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_interpret_ModuleManagement_interface
#define SIMODO_interpret_ModuleManagement_interface

/*! \file ModuleManagement_interface.h
    \brief Интерфейс управления модулями.
*/

#include "simodo/interpret/SemanticModuleFactory_interface.h"
#include "simodo/interpret/SemanticDataCollector_interface.h"
#include "simodo/variable/Module_interface.h"

#include "simodo/interpret/InterpretType.h"
#include "simodo/variable/Variable.h"

#include <memory>
#include <iostream>

namespace simodo::interpret
{
    class Interpret_interface;
}

namespace simodo::interpret
{
    class ModuleManagement_interface
    {
    public:
        virtual ~ModuleManagement_interface() = default;

        virtual void                addSemanticFactories(std::vector<std::shared_ptr<interpret::SemanticModuleFactory_interface>> factories) = 0;
        virtual bool                execute(const std::string & module_name, std::vector<std::string> preload_module_names={}) = 0;

        virtual InterpretType       interpret_type() const = 0;
        virtual SemanticDataCollector_interface & semantic_data() = 0;
        virtual std::ostream &      tout() = 0; ///< for InterpretType::Translate

        virtual bool                isTheModuleLoaded(const std::string & module_name) = 0;
        virtual const ast::Node *   getCode(const std::string & module_name, const inout::uri_set_t & files) = 0;
        virtual std::shared_ptr<variable::Module_interface>
                                    registerSoftModule(const std::string & module_name, std::shared_ptr<variable::Object> module) = 0;
        virtual std::shared_ptr<variable::Object>
                                    produceObject(const std::string & module_name,
                                            interpret::Interpret_interface * interpret = nullptr) = 0;

        virtual const std::string & last_error() const = 0;
    };

}

#endif // SIMODO_interpret_ModuleManagement_interface
