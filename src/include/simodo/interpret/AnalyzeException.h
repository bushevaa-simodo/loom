/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_interpret_AnalyzeException
#define simodo_interpret_AnalyzeException

/*! \file AnalyzeException.h
    \brief Семантическое исключение
*/

#include "simodo/bormental/DrBormental.h"
#include "simodo/inout/token/TokenLocation.h"

namespace simodo::interpret
{
    class AnalyzeException : public bormental::DrBormental
    {
        inout::Location _location;

    public:
        AnalyzeException(std::string where, inout::Location location, std::string what)
            : bormental::DrBormental(where, what)
            , _location(location)
        {}

        const inout::Location & location() const { return _location; }
    };
}

#endif // simodo_interpret_AnalyzeException
