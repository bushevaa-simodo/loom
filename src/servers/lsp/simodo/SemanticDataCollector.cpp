#include "SemanticDataCollector.h"

#include <cassert>

using namespace simodo;

void SemanticDataCollector::collectNameInitiated(const variable::Variable & var)
{
    for(size_t i=_declared.size()-1; i < _declared.size(); --i)
        if (_declared[i].name() == var.name()) {
            _declared[i].value() = var.value();
            return;
        }

    assert(false);
}