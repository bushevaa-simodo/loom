#ifndef ScriptSemanticTokenLegend_h
#define ScriptSemanticTokenLegend_h

/// \file ScriptSemanticTokenLegend.h
/// \brief Перечисления для SemanticToken-запросов. Актуальны только для конкретного языкового сервера.
/// \see https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocument_semanticTokens

#include "simodo/lsp-client/ServerCapabilities.h"

// \todo Добавить описание типов и модификаторов

using simodo::variable::bitShift;

enum class SemanticTokenType : int64_t
{
    Variable      = 0,
    Property      = 1,
    Namespace     = 2,
    BuiltInModule = 3,
    Struct        = 4,
    Function      = 5,
    Method        = 6,
    String        = 7,
    Contract      = 8,
    Module        = 9,
    Type          = 10,
    Keyword       = 11,
    Number        = 12,
    Boolean       = 13,
};

enum class SemanticTokenModifier : int64_t
{
    Anchor      = bitShift(0),  // 01
    Declaration = bitShift(1),  // 02
    Readonly    = bitShift(2),  // 04
    Parameter   = bitShift(3),  // 08
    Input       = bitShift(4),  // 16
    Output      = bitShift(5),  // 32
    Remote      = bitShift(6),  // 64
};

const simodo::lsp::SemanticTokensLegend SCRIPT_SEMANTIC_TOKENS_LEGEND = {
        .tokenTypes = {
                u"variable",        // 00
                u"property",        // 01
                u"namespace",       // 02
                u"built-in-module", // 03
                u"struct",          // 04
                u"function",        // 05
                u"method",          // 06
                u"string",          // 07
                u"contract",        // 08
                u"module",          // 09
                u"type",            // 10
                u"Keyword",         // 11
                u"Number",          // 12
                u"Boolean",         // 13
        },
        .tokenModifiers = {
                u"anchor",          // 01
                u"declaration",     // 02
                u"readonly",        // 04
                u"parameter",       // 08
                u"input",           // 16
                u"output",          // 32
                u"remote",          // 64
        },
};

#endif //ScriptSemanticTokenLegend_h
