#ifndef Input_Supplier_h
#define Input_Supplier_h

#include "simodo/parser/fuze/FuzeRdp.h"
#include "simodo/inout/token/InputStream_interface.h"
#include "simodo/inout/token/InputStreamSupplier_interface.h"
#include "simodo/lsp-server/ServerContext.h"
#include "simodo/lsp-server/DocumentContext.h"

#if __cplusplus >= __cpp_2017
#include <filesystem>
namespace fs = std::filesystem;
#else
#include <experimental/filesystem>
namespace fs = std::filesystem::experimental;
#endif

class InputSupplier : public simodo::inout::InputStreamSupplier_interface
{
    const simodo::lsp::ServerContext &  _server;
    std::string                         _base_file_path;    ///< Путь на основной файл (заданный для разбора), 
                                                            ///< чтобы избегать блокировок
    const std::u16string &              _text; 

public:
    InputSupplier() = delete;
    InputSupplier(const simodo::lsp::ServerContext & server, std::string base_file_path, const std::u16string & text)
        : _server(server) 
        , _base_file_path(std::move(base_file_path))
        , _text(text)
    {}

    virtual std::shared_ptr<simodo::inout::InputStream_interface> supply(const std::string & path) override;
};

#endif