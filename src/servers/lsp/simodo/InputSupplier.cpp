#include "InputSupplier.h"
#include "simodo/inout/token/RefBufferStream.h"
#include "simodo/inout/token/BufferStream.h"
#include "simodo/inout/token/FileStream.h"

using namespace simodo;

std::shared_ptr<inout::InputStream_interface> InputSupplier::supply(const std::string & path)
    {
        if (path == _base_file_path) {
            std::shared_ptr<inout::InputStream_interface> stream = std::make_shared<inout::RefBufferStream>(_text.data());
            return stream;
        }

        if (const lsp::DocumentContext * doc = _server.findDocument(path); doc) {
            /// \note Такая последовательность нужна для выполнения разграничения доступа внутри методов
            /// классов пространства имён lsp
            std::shared_ptr<inout::InputStream_interface> stream = std::make_shared<inout::BufferStream>();
            doc->copyContent(dynamic_cast<inout::BufferStream *>(stream.get())->buffer());
            return stream;
        }

        return std::make_shared<inout::FileStream>(path);
    }