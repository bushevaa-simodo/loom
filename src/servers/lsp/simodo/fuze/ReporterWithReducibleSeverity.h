#ifndef ReporterWithReducibleSeverity_h
#define ReporterWithReducibleSeverity_h

#include "simodo/inout/reporter/Reporter_abstract.h"

class ReporterWithReducibleSeverity: public simodo::inout::Reporter_abstract
{
    simodo::inout::Reporter_abstract &  _m;
    bool                                _reduce_severity = false;

public:
    ReporterWithReducibleSeverity() = delete;
    ReporterWithReducibleSeverity(simodo::inout::Reporter_abstract & m);

    virtual void report(const simodo::inout::SeverityLevel level,
                        const simodo::inout::Location & location,
                        const std::string & briefly,
                        const std::string & atlarge) override;

    void reduceSeverity(bool reduce = true);
};

#endif
