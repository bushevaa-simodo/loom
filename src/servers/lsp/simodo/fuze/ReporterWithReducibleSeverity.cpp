#include "ReporterWithReducibleSeverity.h"

using namespace simodo;

ReporterWithReducibleSeverity::ReporterWithReducibleSeverity(inout::Reporter_abstract & m) : _m(m) {}

void ReporterWithReducibleSeverity::report(const inout::SeverityLevel level,
                        const inout::Location & location,
                        const std::string & briefly,
                        const std::string & atlarge)
    {
        int16_t severity = static_cast<int16_t>(level);
        if (_reduce_severity)
            severity --;
        _m.report(static_cast<inout::SeverityLevel>(severity), location, briefly, atlarge);
    }

void ReporterWithReducibleSeverity::reduceSeverity(bool reduce) { _reduce_severity = reduce; }