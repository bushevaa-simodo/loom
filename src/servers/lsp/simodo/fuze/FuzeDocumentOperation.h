#ifndef FuzeDocumentOperation_h
#define FuzeDocumentOperation_h

#include "script/ScriptDocumentOperation.h"

#include "simodo/loom/Loom.h"
#include "simodo/parser/Grammar.h"

inline const std::u16string SLR_method_report = u"SLR method report";
inline const std::u16string LR1_method_report = u"LR(1) method report";

class FuzeDocumentOperation : public ScriptDocumentOperation
{
    simodo::lsp::DocumentContext &      _doc;
    std::string                         _grammar_dir;
    simodo::variable::VariableSet_t     _hosts_and_operations;
    simodo::loom::Loom                  _loom;
    simodo::parser::Grammar             _grammar;
    std::multimap<std::u16string,std::pair<simodo::inout::Token, std::vector<simodo::inout::Token>>>
                                        _productions;
    std::vector<simodo::inout::Token>   _directions;

public:
    FuzeDocumentOperation(simodo::lsp::DocumentContext & doc, const DocumentOperationFactory & factory, const std::string & languageId);

    virtual bool analyze(const std::u16string & text, simodo::inout::Reporter_abstract & reporter) override;
    virtual bool checkDependency(const std::string & uri) const override;
    virtual simodo::variable::Value produceSimodoCommandResponse(const std::u16string & command_name, std::u16string text) const override;
    virtual simodo::variable::Value produceHoverResponse(const simodo::lsp::Position & pos) const override;
    virtual simodo::variable::Value produceGotoDeclarationResponse(const simodo::lsp::Position & pos) const override;
    virtual simodo::variable::Value produceGotoDefinitionResponse(const simodo::lsp::Position & pos) const override;
    virtual simodo::variable::Value produceCompletionResponse(const simodo::lsp::CompletionParams & completionParams) const override;
    virtual simodo::variable::Value produceSemanticTokensResponse() const override;
    virtual simodo::variable::Value produceDocumentSymbolsResponse() const override;

private:
    std::u16string makeHover(const simodo::inout::Token & t) const;
};

#endif
