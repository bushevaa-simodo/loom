cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

project(simodo-grammatize)

add_executable(${PROJECT_NAME} main.cpp)

set_target_properties(${PROJECT_NAME} PROPERTIES
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS YES
)
target_link_libraries(${PROJECT_NAME} 
    SIMODO-parser
    SIMODO-ast
    SIMODO-interpret
    SIMODO-loom
    SIMODO-utility
    SIMODO-variable
)

set_target_properties(${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

