﻿configure_file(
    ${CMAKE_CURRENT_LIST_DIR}/plugins/StarsInterpreter/runner-setup.json.cmakein
    ${CMAKE_CURRENT_LIST_DIR}/plugins/StarsInterpreter/runner-setup.json
    @ONLY
)

configure_file(
    ${CMAKE_CURRENT_LIST_DIR}/shell/lsp/simodo-json.json.cmakein
    ${CMAKE_CURRENT_LIST_DIR}/shell/lsp/simodo-json.json
    @ONLY
)

configure_file(
    ${CMAKE_CURRENT_LIST_DIR}/shell/lsp/simodo-lsp.json.cmakein
    ${CMAKE_CURRENT_LIST_DIR}/shell/lsp/simodo-lsp.json
    @ONLY
)

configure_file(
    ${CMAKE_CURRENT_LIST_DIR}/shell/lsp/simodo-stars-client.json.cmakein
    ${CMAKE_CURRENT_LIST_DIR}/shell/lsp/simodo-stars-client.json
    @ONLY
)
