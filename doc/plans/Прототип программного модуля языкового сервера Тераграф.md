# Прототип программного модуля языкового сервера Тераграф 

Калягин Иван

## Перечень требований

1. Формирование инфраструктуры (заготовки) программного модуля языкового сервера (есть)
2. Реализация запроса получения диагностики (есть)
3. Реализация запроса получения отчёта о работе языкового сервера
4. Реализация настроек возможностей языкового сервера
5. Анализ описания грамматик для использования в редакторе с применением протокола LSP: 
   а) диагностика ошибок (есть); 
   б) символы документа; 
   в) семантические токены; 
   г) информация о символе в редакторе при наведении мышки; 
   д) переход к определению символа; 
   е) информация для автозавершения ввода.

"Моя работа выполнялась(-естся) в рамках программы 'Приоритет-2030', в проекте под руководством Фетисова М.В."